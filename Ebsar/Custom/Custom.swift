//
//  CustomOutlinedTextField.swift
//  Sokyakom
//
//  Created by Ghoost on 9/25/20.
//


import UIKit

enum ChooseFont:String {
    case Bold = "Tajawal-Bold"
    case Regular = "Tajawal-Regular"
    case Medium = "Tajawal-Medium"
}

func CustomFont(index: ChooseFont) -> String {
    switch index {
    case .Bold:
        return "Tajawal-Bold"
        
    case .Regular:
        return "Tajawal-Regular"
        
    case .Medium:
        return "Tajawal-Medium"
    }
    
}
