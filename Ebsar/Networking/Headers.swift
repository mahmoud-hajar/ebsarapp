

import Foundation
import Alamofire
class Headers {
    
    
    class func getHeader()-> HTTPHeaders{
        //   API.api.token = UserDefaults.standard.object(forKey: "token") as! String
        let dict:HTTPHeaders = [
            .accept("application/json"),
            .contentType("application/json"),
            .authorization("Bearer \(Helper.instance.getUserToken())")
        ]
        return  dict
    }
    
    class func getHeader1()-> HTTPHeaders{
        let dict:HTTPHeaders = [
            .authorization("Bearer \(Helper.instance.getUserToken())")
        ]
        return  dict
    }
    
    
    class func getUpdateLogoHeader()->HTTPHeaders {
        let dict:HTTPHeaders = [
            .accept("application/json"),
            .contentType("multipart/form-data"),
            .authorization("Bearer \(Helper.instance.getUserToken())")
        ]
        return  dict
    }
    
    
    
    // MARK: home Headers
    class func getHomeHeader()-> HTTPHeaders {
        let dict:HTTPHeaders = [
            .accept("application/json"),
            .contentType("application/json"),
        ]
        return  dict
    }
    
    class func getLoginHeader()-> HTTPHeaders {
        let dict:HTTPHeaders = [
            .contentType("application/x-www-form-urlencoded")
        ]
        return  dict
    }
    
    class func pdfHeader()-> HTTPHeaders {
        let dict: HTTPHeaders = [
            .accept("application/pdf"),
            .contentType("application/pdf")
           ]
        return dict
    }
    
    
}
