//Gmail
//
//ebsarcentar@gmail.com
//aass@123456
//
//Link post main
// https://documenter.getpostman.com/view/15195709/TzJsfHqZ
//
//Link xd
//
//https://xd.adobe.com/view/eb084a3c-d4e1-418c-b562-c9422733b89e-be5b/screen/5911ef76-fe13-4a22-85c0-f16fdffc545c/

import Foundation


class URLs {

    static let mainURL = "https://ebsarworldcenter.com/api/"
    static let mainImageURL = "https://ebsarworldcenter.com/storage/"
    static let phoneToken = mainURL + "firebase-tokens"
    
    static let loginURL = mainURL + "client-login"
    static let settingsURL = mainURL + "settings"
    static let registerURL = mainURL + "client-register"
    static let myAppointmentsURL = mainURL + "appointments"
    static let mynotificationsURL = mainURL + "notifications"
    static let myRequestsAppURL = mainURL + "requestsApp"
    static let slidersURL = mainURL + "sliders"
    static let requestsApp = mainURL + "requestsApp?user_id=\(Helper.instance.getUserId())"
    static let myTrreatments = mainURL + "drugs-by-date?orderBy=asc&user_id=\(Helper.instance.getUserId())&date="
    static let myDrugs = mainURL + "drugs?orderBy=desc&user_id=\(Helper.instance.getUserId())"
    static let operations = mainURL + "operations?orderBy=desc&user_id=\(Helper.instance.getUserId())"
    static let questions = mainURL + "questions?orderBy=asc&user_id=\(Helper.instance.getUserId())"
    static let medical_advices = mainURL + "medical_advices?orderBy=asc"
    static let setting = mainURL + "settings"
    static let clientLogout = mainURL + "client-logout"
    static let contactUs = mainURL + "contact-us"
    static let myProfile = mainURL + "client-show-profile"
    static let updateProfile = mainURL + "client-update-profile"
    static let AddNewRequest = mainURL + "AddNewRequest"
}
