

import UIKit
import Alamofire
import SwiftyJSON
import PDFKit

class APIs: NSObject {
    private override init() {}
    static let instance = APIs()
    
    func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    func genericApi<T:Decodable, E:Decodable>( pageNo:Int,url:String,method:HTTPMethod,paameters:Parameters?,headers:HTTPHeaders?,completion:@escaping (T?,E?,Error?) -> ()) {
        AF.request(url, method: method,parameters:paameters,encoding: URLEncoding.default ,headers: headers, interceptor: CustomInterceptor()).validate(statusCode: 200..<300).responseJSON { (response) in
             print(response.response?.statusCode)
            switch response.result{
            case .success(_):
                guard let data=response.data else {return}
                 print(JSON(data))
                do{
                    let data = try JSONDecoder().decode(T.self, from: data)
                    completion(data, nil, nil)
                }catch{
                    print("errrror",error.localizedDescription)
                    //    LottieHelper.shared.hideAnimation()
                    completion(nil, nil, nil)
                }
            case .failure(let error):
                // LottieHelper.shared.hideAnimation()
                //    print(error.localizedDescription)
                guard let data = response.data else { return }
                 print(JSON(data))
                guard let statusCode = response.response?.statusCode else { return }
                // print(statusCode)
                switch statusCode {
                case 400..<500:
                    do {
                        let err = try JSONDecoder().decode(E.self, from: data)
                        completion(nil, err, nil)
                    } catch let jsonError {
                        print(jsonError.localizedDescription)
                    }
                default:
                    completion(nil, nil, error)
                }
     }}}
    
    
     func requestAsRaw<T:Decodable, E:Decodable>(url:String , method: HTTPMethod , headers:HTTPHeaders?,params : Parameters? , completion: @escaping(T?, E?, Error?)->()){

        let url = URL(string: url)
        var request = URLRequest(url: url!)
        request.method = method
        // request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.headers = headers ?? [:]
        if (!JSONSerialization.isValidJSONObject(params ?? [:])) {
            print("is not a valid json object")
            return
        }
        request.httpBody = try! JSONSerialization.data(withJSONObject: params ?? [:])
        AF.request(request).validate(statusCode: 200..<300).responseJSON { response in
          //   print(response.response?.statusCode)

                switch response.result{
                case .success(_):
                    guard let data=response.data else {return}
          //          print(JSON(data))
                    do{
                        let data = try JSONDecoder().decode(T.self, from: data)
                        completion(data, nil, nil)
                    }catch{
              //          print("errrror",error.localizedDescription)
             //           LottieHelper.shared.hideAnimation()
                        completion(nil, nil, nil)
                    }
                case .failure(let error):
              //      LottieHelper.shared.hideAnimation()
                  //  print(error.localizedDescription)
                    guard let data = response.data else { return }
                    guard let statusCode = response.response?.statusCode else { return }
//                    print(statusCode)
//                    print(JSON(data))
                    switch statusCode {
                    case 400..<500:
                        do {
                            let err = try JSONDecoder().decode(E.self, from: data)
                            completion(nil, err, nil)
                        } catch let jsonError {
                            print(jsonError.localizedDescription)
                        }
                    default:
                        completion(nil, nil, error)
                    }
        }}}
        
    
    func requestMultiData<T:Decodable, E:Decodable>(url:String,
                          image:UIImage,
                          params:Parameters?,
                          header:HTTPHeaders?,
                          completion: @escaping(T?, E?, Error?)->()) {
       let urlString = url
        AF.upload( multipartFormData: { multipartFormData in
            if params?.isEmpty == false {
                for (key, value) in params!{
                    if value is String || value is Int{
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
            }}}
            if image.size.width != 0 {
                multipartFormData.append(image.jpegData(compressionQuality: 0.6)!, withName: "logo", fileName: "logo.jpeg", mimeType: "image/jpeg")
        }}, to: urlString,method: .post, headers: header, interceptor: CustomInterceptor()).validate(statusCode: 200..<300).responseJSON { (response) in
        switch response.result{
          case .success(_):
            guard let data=response.data else {return}
              print(JSON(data))
                do{
                  let data = try JSONDecoder().decode(T.self, from: data)
                  completion(data, nil, nil)
                }catch{
                    print("errrror",error.localizedDescription)
                    completion(nil, nil, nil)
                }
          case .failure(let error):
                //    print(error.localizedDescription)
            guard let data = response.data else { return }
            print(JSON(data))
          guard let statusCode = response.response?.statusCode else { return }
        switch statusCode {
            case 400..<500:
                do {
                    let err = try JSONDecoder().decode(E.self, from: data)
                        completion(nil, err, nil)
                } catch let jsonError {
                        print(jsonError.localizedDescription)
                }
        default:
            completion(nil, nil, error)
    }}}}
   
    
    func downloadPdf(uniqueName:String,pdfReport: String,
                     completionHandler:@escaping(String, Bool)->()){
        let downloadUrl: String = URLs.mainImageURL + pdfReport
        //DownloadFileDestination
        let destinationPath: DownloadRequest.Destination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
            let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        print(downloadUrl)
        AF.download(downloadUrl, to: destinationPath)
            .downloadProgress { progress in
                print("progress ==== \(progress)")
            }
            .responseData { response in
                print("response: \(response)")
                switch response.result{
                case .success:
                    if response.fileURL != nil,
                       let filePath = response.fileURL?.absoluteString {
                        completionHandler(filePath, true)
                    }
                    break
                case .failure:
                    completionHandler("", false)
                    break
                }
            }
      }
    
    
}


