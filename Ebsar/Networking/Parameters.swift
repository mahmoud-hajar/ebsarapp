


import Foundation
import Alamofire

class parameters:NSObject {
    private override init() {}
    static let instance=parameters()
    
    func loginParameters(phone:String,password:String,phone_code:String)->Parameters{
        let pameters : [String:Any] = [
            "phone" : phone,
            "password" : password,
            "phone_code" : phone_code
        ]
        return pameters
    }
    
    
    func registerParameters(name:String,phone_code:String,password:String, phone:String,gender:String,birthday:String,software_type:String)->Parameters{
        let pameters : [String:Any] = [
            "name":name,
            "phone_code":phone_code,
            "phone" : phone,
            "password" : password,
            "gender" : gender,
            "birthday" : birthday,
            "software_type":software_type
        ]
        return pameters
    }
    
    func logoutParameters()->Parameters{
        let pameters : [String:Any] = [
            "user_id" : Helper.instance.getUserId(),
            "phone_token" : Helper.instance.getFirebaseToken()
        ]
        return pameters
    }
    
    func contactUsParameters(name:String,email:String, mobile:String,message:String)->Parameters{
        let pameters:[String:Any] = [
            "name" : name,
            "email":email,
            "mobile":mobile,
            "message":message
        ]
        return pameters
    }
    
    func userIdParameter()->Parameters{
        let pameters : [String:Any] = [
            "user_id" : Helper.instance.getUserId(),
        ]
        return pameters
    }
    
 
    func updateParameters(name:String,phone_code:String, phone:String,gender:String,birthday:String,software_type:String)->Parameters{
        let pameters : [String:Any] = [
            "user_id": Helper.instance.getUserId(),
            "name":name,
            "phone_code":phone_code,
            "phone" : phone,
            "gender" : gender,
            "birthday" : birthday
        ]
        return pameters
    }
    
    func requestVacationParameters(name:String,hoppy:String, reason:String,file_number:String,destination:String)->Parameters{
        let pameters : [String:Any] = [
            "user_id": Helper.instance.getUserId(),
            "full_name":name,
            "hoppy":hoppy,
            "reason" : reason,
            "file_number" : file_number,
            "destination" : destination
        ]
        return pameters
    }
    
    func FirebaseTokenParameters()->Parameters{
        let pameters:[String:Any] = [
            "phone_token" : Helper.instance.getFirebaseToken(),
            "user_id":Helper.instance.getUserId(),
            "software_type":"ios"
        ]
        return pameters
    }
    
    
    
    
}
