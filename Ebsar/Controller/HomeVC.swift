

import UIKit
import ImageSlideshow
import SDWebImage

class HomeVC: UIViewController {

    @IBOutlet weak var sliderImage: ImageSlideshow!{
        didSet{
            self.sliderImage.setSliderConfig()
    }}
    @IBOutlet weak var homeImage: UIImageView!{
        didSet{
            self.homeImage.LocalizedHomeImage()
            self.homeImage.addActionn(vc: self, action: #selector(viewsAction(_:)))
    }}
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var wts = ""
    var sliderImages = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionViewConfig()
        self.sliderAPI()
        self.settingAPI()
    }
    

    @objc private func viewsAction(_ sender:AnyObject) {
        Vibration.light.vibrate()
        SoicalHelper.shared.openWhatsapp(phone: self.wts)
    }
   
    @IBAction func ButtonsAction(_ sender: UIButton) {
        sender.springAnimate()
        Vibration.light.vibrate()
        switch sender.tag {
           case 0: performSegue(withIdentifier: "MorSegue", sender: self)
           case 1: performSegue(withIdentifier: "NotifactionSegue", sender: self)
        default:
            return
    }}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MorSegue" {
            let con = segue.destination as! MoreVC
            con.hidesBottomBarWhenPushed = true
    }else  if segue.identifier == "ElmahzuratSegue" {
        let con = segue.destination as! OperationsVC
        con.hidesBottomBarWhenPushed = true
    }else  if segue.identifier == "LeaveOrReportRequestSegue" {
        let con = segue.destination as! LeaveOrReportRequestVC
        con.hidesBottomBarWhenPushed = true
    }else  if segue.identifier == "MedicalAdviceSegue" {
        let con = segue.destination as! Medical_Advice_VC
        con.hidesBottomBarWhenPushed = true
    }else  if segue.identifier == "TheMostCommonQuestionsSegue" {
        let con = segue.destination as! The_most_common_questions
        con.hidesBottomBarWhenPushed = true
    }else  if segue.identifier == "NotifactionSegue" {
        let con = segue.destination as! NotifactionVC
        con.hidesBottomBarWhenPushed = true
        con.recWts = wts
    }}

    
}
//MARK:- CollectionView Config
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource{
    
    private func collectionViewConfig() {
        collectionView.registerCell(cellClass: HomeCollectionCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.setCollectionLayOut(117, 2)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeue(indexPath: indexPath) as HomeCollectionCell
        switch indexPath.row  {
        case 0:
            cell.image.image = UIImage(named: "flame-the-man-got-sick")
            cell.label.text = "Prohibitions".localized()
            
        case 1:
            cell.image.image = UIImage(named: "undraw_Hiring_re_yk5n")
            cell.label.text = "LeaveOrreportRequest".localized()
        case 2:
            cell.image.image = UIImage(named: "2454170")
            cell.label.text = "medicalInformation".localized()
        case 3:
            cell.image.image = UIImage(named: "undraw_Questions_re_1fy7")
            cell.label.text = "TheMostCommonQuestion".localized()
        default: break
        }
        return cell
     }
    
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Vibration.selection.vibrate()
     switch indexPath.row  {
       case 0: performSegue(withIdentifier: "ElmahzuratSegue", sender: self)
       case 1: performSegue(withIdentifier: "LeaveOrReportRequestSegue", sender: self)
       case 2: performSegue(withIdentifier: "MedicalAdviceSegue", sender: self)
       case 3: performSegue(withIdentifier: "TheMostCommonQuestionsSegue", sender: self)
        default: break
        
        }
    }
    
    
}
//MARK:- Networking
extension HomeVC {
    private func sliderAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.slidersURL + "?orderBy=desc", method: .get, paameters: nil, headers: nil) { (data:SliderModel?, errorModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200{
                if data?.data?.count ?? 0 > 0 {
                    self.sliderImages.removeAll()
                    for i in data?.data ?? []{
                        self.sliderImages.append(i.image ?? "")
                    }
                    self.sliderImage.slideShowData(self.sliderImages)
    }}}}
    
    private func settingAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.setting, method: .get, paameters: nil, headers: nil) { (data:SettingModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.wts = data?.data?.whatsapp ?? ""
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
    
}
