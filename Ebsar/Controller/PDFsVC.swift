//
//  PDFsVC.swift
//  Ebsar
//
//  Created by Ghoost on 5/11/21.
//

import UIKit
import PDFKit
import WebKit

class PDFsVC: UIViewController {

    @IBOutlet private weak var tableView: UITableView!{
        didSet {
            self.tableView.registerNib(cell: PdfCell.self)
    }}
    
    var links:[URL]?
    let pdfView = PDFView()
    var fileName: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getPDFs()
        
    }
    

    private func getPDFs() {
        let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last)
        do {
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles))
            self.links = contents
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }} catch{
            print(error.localizedDescription)
        }
    }
    
    

}
//MARK:- TableView Configs
extension PDFsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return links?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeue() as PdfCell
        cell.titleLab.text = links?[indexPath.row].lastPathComponent
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           cell.animateCellFadeInOut()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            Vibration.selection.vibrate()
            self.openPDF(path: (links?[indexPath.row].path)!)
    }
}
//MARK:- Setup PDF Viewer
extension PDFsVC:UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self//or use return self.navigationController for fetching app navigation bar colour
    }
    private func openPDF(path:String) {
        let dc = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
           dc.delegate = self
           dc.presentPreview(animated: true)
    }
    
}
