

import UIKit

class SplashVC: UIViewController {
    let dev = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Helper.instance.checkUserId() == true {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeID")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.present(vc, animated: true, completion: nil)
            }
        }else{
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginID")
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
    }

