
import UIKit
//import iOSDropDown
import MOLH
import DropDown
import FlagPhoneNumber

protocol IsUpdateDelegate {
    func isUpdate(isUpdated:Bool)
}
class RegisterVC: UIViewController {
    @IBOutlet weak var bkImage: UIImageView!{
        didSet{
            self.bkImage.LocalizedImage()
            self.bkImage.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet weak var loginStack: UIStackView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var orView: DesignView!
    @IBOutlet weak var passConstrain: NSLayoutConstraint!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var profileImg: UIImageView!{
        didSet{
            self.profileImg.layer.cornerRadius = profileImg.frame.height/2
        }}
    @IBOutlet weak var creatAnAccountLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!{
        didSet{
            self.loginLabel.text = "login".localized()
        }}
    @IBOutlet weak var youAlreadyHaveAnAccountLabel: UILabel!{
        didSet{
            self.youAlreadyHaveAnAccountLabel.text = "youAlreadyHaveAnAccount".localized()
        }}
    @IBOutlet weak var orLabel: UILabel!{
        didSet{
            self.orLabel.text = "or".localized()
        }}
    
    // TextFields
    @IBOutlet weak var userNameTF: UITextField!{
        didSet{
            self.userNameTF.placeholder = "userName".localized()
        }}
    @IBOutlet weak var PhoneTF: FPNTextField!{
        didSet{
            self.PhoneTF.placeholder = "phone".localized()
        }}
    @IBOutlet weak var passwordTF: UITextField!{
        didSet{
            self.passwordTF.placeholder = "password".localized()
        }}
    @IBOutlet private weak var ageTF: UITextField!{
        didSet{
            self.ageTF.placeholder = "age".localized()
            self.ageTF.setInputViewDatePicker(target: self, selector: #selector(dateChanged))
        }}
    @IBOutlet private weak var genderTF:DropDown!{
        didSet{
//            self.genderTF.placeholder = "genedr".localized()
        }}
    

    
    @IBOutlet weak var userImageView: DesignView!{
        didSet{
            self.userImageView.addActionn(vc: self, action: #selector(getImage))
        }}
    
    @IBOutlet weak var loginView: UIView!{
        didSet{
            self.loginView.addActionn(vc: self, action: #selector(BackLoginVC))
        }}
    @IBOutlet weak var contactWhatsView: DesignView!{
        didSet{
            self.contactWhatsView.addActionn(vc: self, action: #selector(contactWhatsApp))
        }}
    
    //Button
    @IBOutlet weak var creatPress: UIButton!{
        didSet{
            self.creatPress.initButton(title: "creat".localized(), titleColor: .white, backgroundColor: .accentColor, roundValue: 14, index: .Bold, fontSize: 16)
        }}
    @IBOutlet weak var genderPress: UIButton!{
        didSet{
            self.genderPress.initButton(title: "genedr".localized(), titleColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), backgroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), roundValue: 5, index: .Medium, fontSize: 16)
        }}
    
    
    
    let lang =  MOLHLanguage.currentAppleLanguage()
    private var imagePicker: ImagePicker!
    var userImage = UIImage()
    
    var gender_ar = ["انثي","ذكر"]
    var gender_en = ["male","female"]
    var gender_name = ""
    
    var namberForWhatsApp = ""
    
    var isUpdate:Bool = false
    var recModel:loginModel?
    var phoneCode:String = "+966"
    var validPhoneNumber = false
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    var delegate:IsUpdateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.checkIsUpdate()
        self.preparePhoneTF()
    }
    
   
    @IBAction func buttonsAction(_ sender: UIButton) {
        Vibration.light.vibrate()
        sender.springAnimate()
        switch sender.tag {
        case 0: // Gender press
            if self.lang == "ar" {
            if gender_ar.count > 0 {
                self.drop(anchor: sender, dataSource: gender_ar) { (item, index) in
                    sender.setTitle(item, for: .normal)
                    if item == "ذكر"{
                        self.gender_name = "male"
                    }else{
                        self.gender_name = "female"
            }}}}else{
                if gender_en.count > 0 {
                    self.drop(anchor: sender, dataSource: gender_en) { (item, index) in
                        sender.setTitle(item, for: .normal)
                        self.gender_name = item
            }}}
        case 1: //creat Press
            if isUpdate == true {
          let para = parameters.instance.updateParameters(name:self.userNameTF.text ?? "",
                                                          phone_code: self.phoneCode ,
                                                          phone: self.PhoneTF.text!,
                                                          gender: gender_name,
                                                          birthday: self.ageTF.text ?? "",
                                                          software_type: "ios")
                self.updateAPI(para: para)
                
            }else{
                if self.validateInputs() == true {
                    self.goToVerfication()
          }}
        default:
            break
    }}
    
    private func checkIsUpdate() {
        if self.isUpdate == true {
            self.userNameTF.text = recModel?.name
            self.PhoneTF.text = recModel?.phone
            self.gender_name = recModel?.gender ?? ""
            self.profileImg.setImage(from: URLs.mainImageURL+(recModel?.logo ?? ""))
            self.contactWhatsView.isHidden = true
            self.loginStack.isHidden = true
            self.lineView.isHidden = true
            self.orView.isHidden = true
            self.passConstrain.constant = 0
            self.passView.isHidden = true
            self.creatAnAccountLabel.text = "updateProfile".localized()
        }else {
            self.bkImage.isHidden = true
            self.creatAnAccountLabel.text = "Creataccount".localized()
        }
    }
 
    
    
    

}
//MARK:- Networking
extension RegisterVC {
    private func updateAPI(para:[String:Any]) {
        APIs.instance.requestMultiData(url: URLs.updateProfile, image: self.userImage, params: para , header: Headers.getUpdateLogoHeader()) { (data:LoginModel?, errModel:ErrorModel?, error:Error?) in
            if data?.status == 200 {
                NotificationBannerHelper.shared.showSuccess(title: "success".localized(), subtitle: data?.message ?? "")
                self.delegate?.isUpdate(isUpdated: true)
                self.navigationController?.popViewController(animated: true)
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: errModel?.message ?? "")
    }}}
}
//MARK:- Image picker
extension RegisterVC: ImagePickerDelegate {
    @objc private func getImage() {
        Vibration.light.vibrate()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.imagePicker.present(from: userImageView)
    }
    func didSelect(image: UIImage?) {
        guard let image = image else {return}
        self.profileImg.image = image
        self.userImage = image
    }
}
//MARK:- Datepicker
extension RegisterVC {
    @objc func dateChanged(_ sender: UIDatePicker?) {
        if let datePicker = self.ageTF.inputView as? UIDatePicker {
            self.ageTF.text = datePicker.date.getFormattedDate(format: "yyyy-MM-dd")
        }
        self.view.endEditing(true)
    }
}
//MARK:- Validation
extension RegisterVC {
    
    private func validateInputs() -> Bool {
        guard let name = userNameTF.text, !name.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: "userNameIsRequired".localized())
            return false
        }
        guard let phone = PhoneTF.text, !phone.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: "MobileNumberIsRequired".localized())
            return false
        }
        guard let pass = passwordTF.text, !pass.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: "passwordIsRequired".localized())
            return false
        }
//      guard let age = ageTF.text, !age.isEmpty else {
//            Vibration.error.vibrate()
//            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: "ageIsRequired".localized())
//            return false
//        }
        
        return true
    }
    
}
//MARK:- Selectors
extension RegisterVC {
    @objc private func BackLoginVC(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginID")
        present(vc, animated: true, completion: nil)
    }
    @objc private func contactWhatsApp() {
        SoicalHelper.shared.openWhatsapp(phone: namberForWhatsApp)
    }
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }
}
//MARK:- Init pages
extension RegisterVC {
    private func goToVerfication() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerficationCodeVC") as! VerficationCodeVC
        vc.code = "+20"
        vc.name = userNameTF.text!
        vc.password = passwordTF.text!
        vc.phone = PhoneTF.text!
        vc.gender = gender_name
        vc.birthday = ageTF.text!
        vc.userImage = userImage
        vc.recPhoneCode = self.phoneCode
        present(vc, animated: true, completion: nil)
    }
}
extension RegisterVC:FPNTextFieldDelegate {
    
    private func preparePhoneTF() {
        self.PhoneTF.setFlag(countryCode: FPNCountryCode.SA)
        PhoneTF.delegate = self
    }
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        present(navigationViewController, animated: true, completion: nil)
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
       // let x = dialCode.replacingOccurrences(of: "+", with: "00")
        phoneCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            validPhoneNumber = false
            self.PhoneTF.text = textField.getRawPhoneNumber()
            
        } else {
            validPhoneNumber = false
            // Do something...
        }
    }
    
    
}
