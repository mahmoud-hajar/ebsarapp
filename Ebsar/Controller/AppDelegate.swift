//
//  AppDelegate.swift
//  Ebsar
//
//  Created by Mohamed on 2/18/21.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import MOLH
import GoogleMaps
import GooglePlaces
import FirebaseMessaging
import FirebaseCore


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let key = "AIzaSyA6QI378BHt9eqBbiJKtqWHTSAZxcSwN3Q"

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        MOLHLanguage.setDefaultLanguage("en") // Defult Language
        MOLH.shared.activate(true)
        self.reset()
        
        FirebaseApp.configure()
        setupNotifications(application)

        GMSPlacesClient.provideAPIKey(key)
        GMSServices.provideAPIKey(key)
        
        
        
        return true
    }

//    private func application(: UIApplication, open url: URL, options : [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool { }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }

}

extension AppDelegate: MOLHResetable {
    func reset() {
        if Helper.instance.checkUserToken() == true {
            let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "main")
            window?.rootViewController = sb
        }else{
            let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginID")
            window?.rootViewController = sb
       }
    }
    
}
//Notifications
extension AppDelegate : MessagingDelegate{
    func setupNotifications(_ application: UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
            print("Firebase registration token: \(fcmToken)")
            Helper.instance.saveFirebaseToken(token: fcmToken ?? "")
        self.phoneTokenAPI()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        
    }
    
}
extension AppDelegate : UNUserNotificationCenterDelegate{
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        if let data = userInfo as? [String:Any]{
            print("foreNoti$$$",data)
            notificationEvents(notification: data)
        }
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let xx = userInfo as? [String:Any] {
            print("BackNoti$$$",xx)
        }
        completionHandler()
    }
    
    func notificationEvents(notification:[String:Any]?){
        //print("notification body ==" , notification ?? [String:Any]())
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newNoti"), object: nil, userInfo: nil)
    }

}
extension AppDelegate {
    private func phoneTokenAPI() {
        if Helper.instance.checkUserToken() == true {
     APIs.instance.requestMultiData(url: URLs.phoneToken, image: UIImage(), params: parameters.instance.FirebaseTokenParameters(), header: Headers.getUpdateLogoHeader()) { (data:ErrorModel?, errModel:ErrorModel?, error:Error?) in
                print("👍🏻 firebase token updated")
    }}}
}
