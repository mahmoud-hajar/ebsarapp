//
//  NotificationDetailsVC.swift
//  Ebsar
//
//  Created by Ghoost on 5/17/21.
//

import UIKit

class NotificationDetailsVC: UIViewController {
    
    @IBOutlet private weak var img: UIImageView!
    @IBOutlet private weak var txtView: UITextView!
    @IBOutlet private weak var bkImage: UIImageView!{
        didSet{
            self.bkImage.LocalizedImage()
            self.bkImage.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet weak var headTitle: UILabel!{
        didSet{
            self.headTitle.text = "notificationDetails".localized()
    }}
    
    var recModel: notificationModel?
    var wts = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtView.text = recModel?.message
        self.img.setImage(from: URLs.mainImageURL + (recModel?.image ?? ""))
        
    }

    @IBAction private func wtsBtn(_ sender: UIButton) {
        Vibration.light.vibrate()
        sender.springAnimate()
        SoicalHelper.shared.openWhatsapp(phone: self.wts)
    }
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }

    
    
    
    
}
