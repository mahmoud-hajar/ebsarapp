//
//  LeaveOrReportRequestVC.swift
//  Ebsar
//
//  Created by Mohamed on 2/22/21.
//

import UIKit

class LeaveOrReportRequestVC: UIViewController {

    // Navigation Bar
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "LeaveOrReportRequest".localized()
    }}
    
    //label
    @IBOutlet private weak var sickLeaveRequestLabel: UILabel!{
        didSet{
            self.sickLeaveRequestLabel.text = "sickLeaveRequestLabel".localized()
    }}
    @IBOutlet private weak var requestAMedicalReportLabel: UILabel!{
        didSet{
            self.requestAMedicalReportLabel.text = "requestAMedicalReportLabel".localized()
    }}
    @IBOutlet private weak var RequestTestsLabel: UILabel!{
        didSet{
            self.RequestTestsLabel.text = "RequestTestsLabel".localized()
    }}
    @IBOutlet weak var glassesMeasurementsRequestLabel: UILabel!{
        didSet{
            self.glassesMeasurementsRequestLabel.text = "glassesMeasurementsRequest".localized()
    }}
    //image
    @IBOutlet private weak var image1: UIImageView!{
        didSet{
            self.image1.LocalizedLeaveOrReportRequestImage()
    }}
    @IBOutlet private weak var image2: UIImageView!{
        didSet{
            self.image2.LocalizedLeaveOrReportRequestImage()
    }}
    @IBOutlet private weak var image3: UIImageView!{
        didSet{
            self.image3.LocalizedLeaveOrReportRequestImage()
    }}
    @IBOutlet weak var image4: UIImageView!{
        didSet{
            self.image4.LocalizedLeaveOrReportRequestImage()
    }}
    //view
    @IBOutlet private weak var sickLeaveRequestView: DesignView!{
        didSet{
            self.sickLeaveRequestView.addActionn(vc: self, action: #selector(viewsAction(_:)))
    }}
    @IBOutlet private weak var requestAMedicalReportView: DesignView!{
        didSet{
            self.requestAMedicalReportView.addActionn(vc: self, action: #selector(viewsAction(_:)))
    }}
    @IBOutlet private weak var requestTestsView: DesignView!{
        didSet{
            self.requestTestsView.addActionn(vc: self, action: #selector(viewsAction(_:)))
    }}
    @IBOutlet weak var glassesMeasurementsRequestView: DesignView!{
        didSet{
            self.glassesMeasurementsRequestView.addActionn(vc: self, action: #selector(viewsAction(_:)))
    }}
    
    
    var selectedTag:Int?
    var actionTag:Int?
    var selectedView:Int?
    var pdfURL:String?
    var notiModel:notificationModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }

   
    
    
    
  
}
//MARK:- Networking
extension LeaveOrReportRequestVC {
    private func checkStatusAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.requestsApp, method: .get, paameters: nil, headers: Headers.getHeader()) { (data:RequestVecationModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.checkTag(data: data)
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
//MARK:- SetupUI
extension LeaveOrReportRequestVC {
    
    private func checkTag(data:RequestVecationModel?) {
        if self.self.selectedView == 1 {
            if data?.vacationReport == nil {
                // json null
                performSegue(withIdentifier: "SickLeaveSegue", sender: self)
            }else if data?.vacationReport?.pdf?.isEmpty == true  || data?.vacationReport?.pdf == nil {
                // pdf null
                performSegue(withIdentifier: "downloadPdfSegue", sender: self)
           }else if data?.vacationReport?.pdf?.isEmpty == false && data?.vacationReport?.requestFk?.fullName?.isEmpty == false {
               // json exsist
            self.actionTag = 1
            self.pdfURL = data?.vacationReport?.pdf ?? ""
            performSegue(withIdentifier: "SickLeaveSegue", sender: self)
        }}else if self.selectedView  == 2{
            self.requestAMedicalReportAction()
        } else if self.selectedView  == 3 {
            self.requestTestsViewAction()
        }else if self.selectedView  == 4 {
            self.glassesMeasurementsRequestViewAction()
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SickLeaveSegue" {
            let vc = segue.destination as! SickLeaveRequestVC
            vc.recActionTag = self.actionTag
            vc.recSelectedView = self.selectedView
            vc.pdfURL = self.pdfURL
            vc.hidesBottomBarWhenPushed = true
        }else if segue.identifier == "downloadPdfSegue" {
            let vc = segue.destination as! DownloadPDFVC
            vc.recActionTag = self.actionTag
            vc.hidesBottomBarWhenPushed = true
        }
    }
    
    
}
//MARK:- Selectors
extension LeaveOrReportRequestVC {
    
    @objc private func viewsAction(_ sender: AnyObject) {
        Vibration.light.vibrate()
        self.selectedView = sender.view.tag
        self.checkStatusAPI()
    }
    
    
    @objc private func sickLeaveRequestAction(){
        Vibration.light.vibrate()
        self.sickLeaveRequestView.springAnime()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DownloadPDF") as! DownloadPDFVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc private func requestAMedicalReportAction(){
        Vibration.light.vibrate()
        self.requestAMedicalReportView.springAnime()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DownloadPDF") as! DownloadPDFVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @objc private func requestTestsViewAction(){
        Vibration.light.vibrate()
        self.requestTestsView.springAnime()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DownloadPDF") as! DownloadPDFVC
        vc.hidesBottomBarWhenPushed = true
        vc.whereButtonPress = "examine_report"
        self.navigationController?.pushViewController(vc, animated: true)


    }
    @objc private func glassesMeasurementsRequestViewAction(){
        Vibration.light.vibrate()
        self.glassesMeasurementsRequestView.springAnime()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DownloadPDF") as! DownloadPDFVC
        vc.hidesBottomBarWhenPushed = true
        vc.whereButtonPress = "glasses_measurement_report"
        self.navigationController?.pushViewController(vc, animated: true)

    }
}
