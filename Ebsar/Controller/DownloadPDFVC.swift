//
//  DownloadPDFVC.swift
//  Ebsar
//
//  Created by Mohamed on 5/4/21.
//

import UIKit
import PDFKit

class DownloadPDFVC: UIViewController {
    
   // Navigation Bar
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
    }}
    
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "".localized()
    }}
    
   // label
    @IBOutlet weak var label: UILabel!{
        didSet{
            self.label.text = "requestWillBe".localized()
        }}
    // buttons
    @IBOutlet weak var DownloadPress: UIButton!{
        didSet{
            self.DownloadPress.setTitle("download".localized(), for: .normal)
        }}
    @IBOutlet weak var showPress: UIButton!{
        didSet{
            self.showPress.setTitle("show".localized(), for: .normal)
        }}
    //view
    @IBOutlet weak var notFoundPDFView: UIView!
    @IBOutlet weak var foundPDF: UIView!
    
    @IBOutlet weak var visualView: UIVisualEffectView!
    @IBOutlet weak var fileNameTF: UITextField!{
        didSet{
            self.fileNameTF.placeholder = "fileName".localized()
        }}
    @IBOutlet weak var downloadBtn: UIButton!{
        didSet{
            self.downloadBtn.initButton(title: "download".localized(), titleColor: .white, backgroundColor: #colorLiteral(red: 0.168627451, green: 0.6941176471, blue: 0.6352941176, alpha: 1), roundValue: 5, index: .Medium, fontSize: 16)
        }}
    
    
    
    var whereButtonPress = ""
    var PdfURL = ""
    var recActionTag:Int?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.visualView.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.myRequestsAppAPI(whereButtonPress: whereButtonPress)
    }
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionButtons(_ sender: UIButton) {
        switch sender.tag {
          case 0:
             UIView.animate(withDuration: 0.3) {
                self.visualView.isHidden = false
             }
          case 1:
             performSegue(withIdentifier: "showPdfSegue", sender: self)
          case 3:
            guard (fileNameTF.text != nil) else {
                Vibration.error.vibrate()
                return
            }
            self.downloadPdfAPI(uniqueName: self.fileNameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
          default: break
        }
    }

}
//
//MARK:- Networking
extension DownloadPDFVC{
    private func myRequestsAppAPI(whereButtonPress : String) {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.myRequestsAppURL+"?user_id=\(Helper.instance.getUserId())", method: .get, paameters: nil, headers: Headers.getHeader1()) { (data:ApplicationForms?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                if whereButtonPress == "examine_report" {
                    // request Tests View press
                    if data?.examineReport == nil || data?.examineReport?.pdf == nil {
                        print("33333333333")
                        self.notFoundPDFView.isHidden = false
                        self.foundPDF.isHidden = true
                        
                    }else if data?.examineReport != nil && data?.examineReport?.pdf != nil {
                        print("4444444444")
                        self.notFoundPDFView.isHidden = true
                        self.foundPDF.isHidden = false
                        self.PdfURL = data?.examineReport?.pdf ?? ""

        }}else  if whereButtonPress == "glasses_measurement_report" {
                    //glasses Measurements Request View press
           
                    if data?.glassesMeasurementReport == nil || data?.glassesMeasurementReport?.pdf == nil {
                        print("55555555555")
                        self.notFoundPDFView.isHidden = false
                        self.foundPDF.isHidden = true

                    }else if data?.glassesMeasurementReport != nil && data?.glassesMeasurementReport?.pdf != nil {
                        print("6666666666666")
                        self.notFoundPDFView.isHidden = true
                        self.foundPDF.isHidden = false
                        self.PdfURL = data?.examineReport?.pdf ?? ""
        }}}else if errModel?.status != 200 {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: errModel?.message ?? "")
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: error?.localizedDescription ?? "ThereIsAProblemWithTheConnection".localized())
    }}}
    
    private func downloadPdfAPI(uniqueName:String) {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.downloadPdf(uniqueName: uniqueName, pdfReport:self.PdfURL) { (filePath:String, status:Bool) in
            LottieHelper.shared.hideAnimation()
            if status == true {
                NotificationBannerHelper.shared.showSuccess(title: "success".localized(), subtitle: "")
                UIView.animate(withDuration: 0.3) {
                    self.visualView.isHidden = true
        }}else{
                UIView.animate(withDuration: 0.3) {
                    self.visualView.isHidden = true
    }}}}


}

//extension DownloadPDFVC:  URLSessionDownloadDelegate {
////    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
////        print("downloadLocation:", location)
////        // create destination URL with the original pdf name
////        guard let url = downloadTask.originalRequest?.url else { return }
////        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
////        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
////        // delete original copy
////        try? FileManager.default.removeItem(at: destinationURL)
////        // copy from temp to Document
////        do {
////            try FileManager.default.copyItem(at: location, to: destinationURL)
////            self.pdfURL = destinationURL
////        } catch let error {
////            print("Copy Error: \(error.localizedDescription)")
////        }
////    }
////

//func savePdff(urlString:String, fileName:String) {
//        DispatchQueue.main.async {
//            let url = URL(string: urlString)
//            let pdfData = try? Data.init(contentsOf: url!)
//            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
//            let pdfNameFromUrl = "YourAppName-\(fileName).pdf"
//            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
//            do {
//                try pdfData?.write(to: actualPath, options: .atomic)
//                print("pdf successfully saved!")
//            } catch {
//                print("Pdf could not be saved")
//            }
//        }
//    }
//
//    func showSavedPdf(url:String, fileName:String) {
//        if #available(iOS 10.0, *) {
//            do {
//                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
//                for url in contents {
//                    if url.description.contains("\(fileName).pdf") {
//                       // its your file! do what you want with it!
//
//                }
//            }
//        } catch {
//            print("could not locate pdf file !!!!!!!")
//        }
//    }
//}
//
//// check to avoid saving a file multiple times
//func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
//    var status = false
//    if #available(iOS 10.0, *) {
//        do {
//            let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//            let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
//            for url in contents {
//                if url.description.contains("YourAppName-\(fileName).pdf") {
//                    status = true
//                }
//            }
//        } catch {
//            print("could not locate pdf file !!!!!!!")
//        }
//    }
//    return status
//}


////}
//        self.notFoundPDFView.isHidden = true
//        self.foundPDF.isHidden = true


//        func savePdf(urlString:String, fileName:String) {
//                DispatchQueue.main.async {
//                    let url = URL(string: urlString)
//                    let pdfData = try? Data.init(contentsOf: url!)
//                    let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
//                    let pdfNameFromUrl = "YourAppName-\(fileName).pdf"
//                    let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
//                    do {
//                        try pdfData?.write(to: actualPath, options: .atomic)
//                        print("pdf successfully saved!")
//        //file is downloaded in app data container, I can find file from x code > devices > MyApp > download Container >This container has the file
//                    } catch {
//                        print("Pdf could not be saved")
//                    }
//                }
//            }
