//
//  LanguageSettingVC.swift
//  Ektfa
//
//  Created by Ghoost on 4/14/21.
//

import UIKit
import MOLH

class LanguageSettingVC: UIViewController {

    @IBOutlet private weak var headTitle: UILabel!{
        didSet{
            self.headTitle.text = "language".localized()
    }}
    @IBOutlet private weak var arView: DesignView!{
        didSet{
            self.arView.tag = 1
            self.arView.addActionn(vc: self, action: #selector(viewActions(_:)))
    }}
    @IBOutlet private weak var enView: DesignView!{
        didSet{
            self.enView.tag = 2
            self.enView.addActionn(vc: self, action: #selector(viewActions(_:)))
    }}
    @IBOutlet private weak var bakImage: UIImageView!{
        didSet{
            self.bakImage.LocalizedImage()
            self.bakImage.addActionn(vc: self, action: #selector(dis))
    }}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
        
    @objc private func dis() {
        Vibration.light.vibrate()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func viewActions(_ sender: UIGestureRecognizer) {
        Vibration.light.vibrate()
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
        MOLH.reset()
    }

   
    

}
