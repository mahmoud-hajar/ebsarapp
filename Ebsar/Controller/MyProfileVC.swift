//
//  MyProfileVC.swift
//  Ebsar
//
//  Created by Mohamed on 2/22/21.
//

import UIKit



class MyProfileVC: UIViewController {
    
    @IBOutlet weak var myAccountLab: UILabel!{
        didSet{
            self.myAccountLab.text = "myAccount".localized()
    }}
    @IBOutlet weak var mainImage: UIImageView!{
        didSet{
            self.mainImage.LocalizedProfileImage()
    }}
    @IBOutlet weak var userImage: UIImageView!{
        didSet{
            self.userImage.setRoundCorners(self.userImage.frame.size.width/2)
    }}
    @IBOutlet weak var personal_Information_Label: UILabel!{
        didSet{
            self.personal_Information_Label.text = "personalInformation".localized()
    }}
 
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var phoneLab: UILabel!
    @IBOutlet weak var genderLab: UILabel!
    @IBOutlet weak var ageLab: UILabel!
    @IBOutlet weak var phoneNumLab: UILabel!{
        didSet{
            self.phoneNumLab.text = "phone".localized()
    }}
    @IBOutlet weak var userGenderLab: UILabel!{
        didSet{
            self.userGenderLab.text = "gender".localized()
    }}
    @IBOutlet weak var userAgeLab: UILabel!{
        didSet{
            self.userAgeLab.text = "birthday".localized()
    }}
    
    
    var userModel: loginModel?
        
        
    override func viewDidLoad() {
        super.viewDidLoad()

          self.profileAPI()
        
    }
    
    @IBAction func edit_Press(_ sender: UIButton) {
        sender.springAnimate()
        Vibration.light.vibrate()
        
        performSegue(withIdentifier: "updateSegue", sender: self)
    }
    
    private func userData(model:loginModel?) {
        self.phoneLab.text = model?.phone
        self.genderLab.text = model?.gender
        self.ageLab.text = model?.birthday
        self.userImage.setImage(from: URLs.mainImageURL+(model?.image ?? ""))
        self.userName.text = model?.name
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateSegue" {
            let vc = segue.destination as! RegisterVC
                vc.delegate = self
                vc.isUpdate = true
                vc.recModel = userModel
                vc.hidesBottomBarWhenPushed = true
        }
    }
    
    
}
//MARK:- Networking
extension MyProfileVC {
    private func profileAPI() {
        LottieHelper.shared.startAnimation(view: view)
    APIs.instance.requestMultiData(url: URLs.myProfile, image: UIImage(), params: parameters.instance.userIdParameter(), header: Headers.getUpdateLogoHeader()) { (data:LoginModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.userModel = data?.data
                self.userData(model: data?.data)
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
extension MyProfileVC:IsUpdateDelegate {
    func isUpdate(isUpdated: Bool) {
        if isUpdated == true {
            self.profileAPI()
        }
    }
}
