//
//  NotifactionVC.swift
//  Ebsar
//
//  Created by Mohamed on 2/28/21.
//

import UIKit

class NotifactionVC: UIViewController {
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
            }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "Notifications".localized()
        }}
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.registerNib(cell: NotifactionCell.self)
            self.tableView.tableFooterView = UIView()
            self.tableView.tableFooterView?.tintColor = .clear
            // to automatic height for cell
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
    }}
    
    var notificationData = [notificationModel]()
    var selectedModel:notificationModel?
    var recWts = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkUser()
        
    }
    

    private func checkUser() {
        if Helper.instance.checkUserId() == true {
           self.appointmentsAPI()
        }else{
           loginAlert()
    }}
    
    private func notiTypeEvents(model:notificationModel?) {
        self.selectedModel = model
        if model?.actionType == "general" {
                 self.selectedModel = model
                 performSegue(withIdentifier: "notiDetailsSegue", sender: self)
        }else if model?.actionType == "appointment" {
            self.tabBarController?.selectedIndex = 2
        }else if model?.actionType == "operation"{
            self.performSegue(withIdentifier: "ElmahzuratSegue", sender: self)
        }else if model?.actionType == "drug" {
            self.tabBarController?.selectedIndex = 1
        }else if model?.actionType == "vacation" ||
                 model?.actionType == "examine" ||
                 model?.actionType == "glasses_measurement" ||
                 model?.actionType == "medical" {
            performSegue(withIdentifier: "notiRequestSegue", sender: self)
        }}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "notiDetailsSegue" {
            let vc = segue.destination as! NotificationDetailsVC
                vc.recModel = self.selectedModel
            vc.wts = self.recWts
        }else if segue.identifier == "LeaveOrReportRequestSegue" {
              let vc = segue.destination as! LeaveOrReportRequestVC
                  vc.notiModel = self.selectedModel
        }}
    
    //Notification Center
    private func observers() {
        NotificationCenter.default.addObserver(self, selector: #selector(notiRecieved), name: NSNotification.Name(rawValue: "newNoti"), object: nil)
    }
    
    //Selectors
    @objc func notiRecieved(_ noti:Notification) {
        self.checkUser()
    }
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
}
//MARK:- tabel config
extension NotifactionVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeue() as NotifactionCell
                 cell.notiModel = notificationData[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
              cell.animateCellFadeInOut()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Vibration.selection.vibrate()
        self.notiTypeEvents(model: notificationData[indexPath.row])
    }
    
    
}
//MARK:- Networking
extension NotifactionVC{
    private func appointmentsAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.mynotificationsURL+"?orderBy=desc"+"&user_id=\(Helper.instance.getUserId())", method: .get, paameters: nil, headers: nil) { (data:NotificationModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                if data?.data != nil {
                    //get data
                    self.notificationData = data?.data ?? []
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
            }}
                if data?.data?.isEmpty ==  true{

                    self.tableView.isHidden = true
                }else{
                    DispatchQueue.main.async {
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
            }} else if errModel?.status != 200 {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: errModel?.message ?? "")
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: error?.localizedDescription ?? "ThereIsAProblemWithTheConnection".localized())
        }}}
}
