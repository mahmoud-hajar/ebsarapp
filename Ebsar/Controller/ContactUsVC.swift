//
//  ContactUsVC.swift
//  Ebsar
//
//  Created by Mohamed on 2/28/21.
//

import UIKit

class ContactUsVC: UIViewController {

    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
        }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "ContactUs".localized()
        }}
    @IBOutlet private weak var nameLabel: UILabel!{
        didSet{
            self.nameLabel.text = "Name".localized()
        }}
    @IBOutlet private weak var emailLabel: UILabel!{
        didSet{
            self.emailLabel.text = "Email".localized()
        }}
    @IBOutlet private weak var phoneLabel: UILabel!{
        didSet{
            self.phoneLabel.text = "Phone".localized()
        }}
    @IBOutlet private weak var messageLabel: UILabel!{
        didSet{
            self.messageLabel.text = "Message".localized()
        }}
    @IBOutlet private weak var nameTF: UITextField!{
        didSet{
            self.nameTF.placeholder = "Name".localized()
        }}
    @IBOutlet private weak var emailTF: UITextField!{
        didSet{
            self.emailTF.placeholder = "Email".localized()
        }}
    @IBOutlet private weak var phoneTF: UITextField!{
        didSet{
            self.phoneTF.placeholder = "Phone".localized()
        }}
    @IBOutlet private weak var messageTF: UITextView!{
        didSet{
            self.messageTF.placeholder = "Message".localized()
        }}
    @IBOutlet private weak var sendButton: UIButton!{
        didSet{
            self.sendButton.initButton(title: "Send".localized(), titleColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), backgroundColor: #colorLiteral(red: 0.155536294, green: 0.6424156427, blue: 0.5638408661, alpha: 1), roundValue: 20, index: .Medium, fontSize: 16)
        }}
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }

    @IBAction func sendButton(_ sender: UIButton) {
        Vibration.light.vibrate()
        sender.springAnimate()
        if self.validateInputs() {
            self.contactUsAPI()
        }
    }
    

}
//MARK:- Validation
extension ContactUsVC {
    private func validateInputs() -> Bool {
        guard let name = nameTF.text, !name.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "NameIsRequired".localized())
            return false
        }
        guard let email = emailTF.text, !email.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "EmailIsRequired".localized())
            return false
        }
       
        guard let phone = phoneTF.text, !phone.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "MobileNumberIsRequired".localized())
        
            return false
        }
        guard let message = messageTF.text, !message.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "MessageIsRequired".localized())
            return false
        }
        
        guard email.isValidEmail == true  else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "EnterTheEmailCorrectly".localized())
            return false
        }
        return true
    }
    
}


//MARK:- Networking
extension ContactUsVC {
    private func contactUsAPI() {
        LottieHelper.shared.startAnimation(view: view)
        let para = parameters.instance.contactUsParameters(name: nameTF.text!, email: emailTF.text!, mobile: phoneTF.text!, message: messageTF.text!)
        APIs.instance.requestMultiData(url: URLs.contactUs, image: UIImage(), params:para, header: nil) { (data:ErrorModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                NotificationBannerHelper.shared.showSuccess(title: "success".localized(), subtitle: "")
                self.navigationController?.popViewController(animated: true)
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
}
