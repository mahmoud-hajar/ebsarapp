

import UIKit
import MOLH
import GoogleMaps

class MoreVC: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!{
        didSet{
            self.mapView.setRoundCorners(12)
            self.mapView.isUserInteractionEnabled = false
    }}
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
        }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "More".localized()
        }}
    @IBOutlet private weak var languageLabel: UILabel!{
        didSet{
            self.languageLabel.text = "language".localized()
        }}
    @IBOutlet weak var curLangLab: UILabel!{
        didSet{
            self.curLangLab.text = "currentLanguage".localized()
        }}
    
    @IBOutlet private weak var AlertsLabel: UILabel!{
        didSet{
            self.AlertsLabel.text = "Alerts".localized()
        }}
    @IBOutlet private weak var TermsAndConditionsLabel: UILabel!{
        didSet{
            self.TermsAndConditionsLabel.text = "TermsAndConditions".localized()
        }}
    
    @IBOutlet private weak var AboutTheAppLabel: UILabel!{
        didSet{
            self.AboutTheAppLabel.text="AboutTheApp".localized()
        }}
    @IBOutlet private weak var ConnectWithUsLabel: UILabel!{
        didSet{
            self.ConnectWithUsLabel.text = "ConnectWithUs".localized()
        }}
    @IBOutlet private weak var OurLocationLabel: UILabel!{
        didSet{
            self.OurLocationLabel.text = "OurLocation".localized()
        }}
    @IBOutlet private weak var SocialMediaLabel: UILabel!{
        didSet{
            self.SocialMediaLabel.text = "socialMediaL".localized()
        }}
    @IBOutlet private weak var signOutLabel: UILabel!{
        didSet{
            self.signOutLabel.text = "signOut".localized()
        }}
    
    @IBOutlet private weak var TermsAndConditionsImage: UIImageView!{
        didSet{
            self.TermsAndConditionsImage.LocalizedMoreImage()
        }}
    @IBOutlet private weak var AboutTheAppImage: UIImageView!{
        didSet{
            self.AboutTheAppImage.LocalizedMoreImage()
        }}
    @IBOutlet private weak var ConnectWithUsImage: UIImageView!{
        didSet{
            self.ConnectWithUsImage.LocalizedMoreImage()
        }}
    
    @IBOutlet private weak var languageView: UIView!{
        didSet{
            self.languageView.addActionn(vc: self, action: #selector(viewsAction(_:)))
        }}
    @IBOutlet private weak var TermsAndConditionsView: UIView!{
        didSet{
            self.TermsAndConditionsView.addActionn(vc: self, action: #selector(viewsAction(_:)))
        }}
    @IBOutlet private weak var AboutTheAppView: UIView!{
        didSet{
            self.AboutTheAppView.addActionn(vc: self, action: #selector(viewsAction(_:)))
        }}
    @IBOutlet private weak var ConnectWithUsView: UIView!{
        didSet{
            self.ConnectWithUsView.addActionn(vc: self, action: #selector(viewsAction(_:)))
        }}
    @IBOutlet private weak var signOutView: UIView!{
        didSet{
            self.signOutView.addActionn(vc: self, action: #selector(viewsAction(_:)))
        }}
    @IBOutlet weak var locationView: UIView!{
        didSet{
            self.locationView.addActionn(vc: self, action: #selector(viewsAction(_:)))
        }}
    
    
    var isTerm:Bool = false
    var wts = ""
    var twitter = ""
    var insta = ""
    var fb = ""
    var long = ""
    var lat = ""
    var marker = GMSMarker()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingAPI()
        
    }
    
    @IBAction func AlertPress(_ sender: UISwitch) {
        
        
    }
    
    @IBAction func actionButtons(_ sender: UIButton) {
        sender.springAnimate()
        Vibration.light.vibrate()
        switch sender.tag {
          case 1:
            SoicalHelper.shared.openTwitter(twitter: self.twitter)
          case 2:
            SoicalHelper.shared.openInstagram(instgram: self.insta)
          case 3:
            SoicalHelper.shared.openWhatsapp(phone: self.wts)
          case 4:
            SoicalHelper.shared.openFacebook(facebook: self.fb)
        default:
            return
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "appInfoSegue" {
            let vc = segue.destination as! AppInfoVC
            vc.isTerms = self.isTerm
        }
    }
    

}
//MARK:- GoogleMaps
extension MoreVC:GMSMapViewDelegate {
    
    private func setLoca(lat:Double, long:Double,regionInMeters:Float) {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: regionInMeters)
        marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: long))
        self.mapView.mapType = .hybrid
        mapView.delegate = self
        marker.map = self.mapView
        mapView.selectedMarker = marker
        DispatchQueue.main.async {
            self.mapView.animate(to: camera)
        }
    }
    
    
}
//MARK:- Selectors
extension MoreVC {
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func viewsAction(_ sender: AnyObject) {
        Vibration.light.vibrate()
        if sender.view.tag == 1 {
            performSegue(withIdentifier: "languageSegue", sender: self)
        }else if sender.view.tag == 2 {
            self.isTerm = true
           performSegue(withIdentifier: "notiSegue", sender: self)
        }else if sender.view.tag == 3 {
            self.isTerm = true
           performSegue(withIdentifier: "appInfoSegue", sender: self)
        }else if sender.view.tag == 4 {
            self.isTerm = false
            performSegue(withIdentifier: "appInfoSegue", sender: self)
        }else if sender.view.tag == 5 {
            performSegue(withIdentifier: "contactUsSegue", sender: self)
        }else if sender.view.tag == 6 {
            //performSegue(withIdentifier: "locationsSegue", sender: self)
            Helper.instance.openMapOnAddress(long: self.long, lat: self.lat)
        }else if sender.view.tag == 7 {
            self.logoutAPI()
        }
    }
    
    
}
//MARK:- Networking
extension MoreVC {
    private func settingAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.setting, method: .get, paameters: nil, headers: nil) { (data:SettingModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.wts = data?.data?.whatsapp ?? ""
                self.fb = data?.data?.facebook ?? ""
                self.twitter = data?.data?.twitter ?? ""
                self.insta = data?.data?.instagram ?? ""
                self.long = "\(data?.data?.longitude ?? 0.0)"
                self.lat = "\(data?.data?.latitude ?? 0.0)"
                self.setLoca(lat: data?.data?.latitude ?? 0.0, long: data?.data?.longitude ?? 0.0, regionInMeters: 11)
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
    private func logoutAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.requestMultiData(url: URLs.clientLogout, image: UIImage(), params: parameters.instance.logoutParameters(), header: Headers.getUpdateLogoHeader()) { (data:ErrorModel?, errModel:ErrorModel?, error:Error?) in
            if data?.status == 200 {
                Helper.instance.deletUserDefaults()
                MOLH.reset()
    }}}
    
}
