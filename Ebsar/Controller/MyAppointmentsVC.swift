

import UIKit

class MyAppointmentsVC: UIViewController {
    var MyAppointmentsData = [myAppointmentsModel]()
// Navigation
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "MyAppointments".localized()
        }
    }
    
    // tableView
    @IBOutlet private weak var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewConfig()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.checkUser()

    }

    private func checkUser() {
        if Helper.instance.checkUserId() == true {
           self.appointmentsAPI()
        }else{
         loginAlert()
    }}

}

//MARK:- TablevView Config
extension MyAppointmentsVC: UITableViewDelegate, UITableViewDataSource {
    private func tableViewConfig() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerNib(cell: MyAppointmentsTabelCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.tableFooterView?.tintColor = .clear
        self.tableView.addRefreshControll(actionTarget: self, action: #selector(refresh(_:)))
    }
    @objc private func refresh(_ refreshontrol: UIRefreshControl) {
        self.checkUser()
        refreshontrol.endRefreshing()
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        130
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MyAppointmentsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeue() as MyAppointmentsTabelCell
      
        cell.model = MyAppointmentsData[indexPath.item]

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
              cell.animateCellFadeInOut()
    }
    
    
    
}
//MARK:- Networking
extension MyAppointmentsVC{
    private func appointmentsAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.myAppointmentsURL+"?orderBy=desc"+"&user_id=\(Helper.instance.getUserId())", method: .get, paameters: nil, headers: nil) { (data:MyAppointmentsModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                if data?.data != nil {
                    //get data
                    self.MyAppointmentsData = data?.data ?? []
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
            }}
                if data?.data?.isEmpty ==  true{
                    print("noooooooo")
                    self.tableView.isHidden = true
                }else{
                    DispatchQueue.main.async {
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
            }
                
                
            }
            else if errModel?.status != 200 {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: errModel?.message ?? "")
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: error?.localizedDescription ?? "ThereIsAProblemWithTheConnection".localized())
        }}}
}
