//
//  ElmahzuratVC.swift
//  Ebsar
//
//  Created by Mohamed on 2/22/21.
//

import UIKit

class OperationsVC: UIViewController {
    
    // Navigation Bar
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "operations".localized()
    }}
    @IBOutlet private weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.registerCell(cellClass: OperationCell.self)
            self.collectionView.setCollectionLayOut(160, 2)
    }}
    
    
    lazy var refreshControl = UIRefreshControl()
    private var operationsData = [operationsModel]()
    var selIndx:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.operationsAPI()
        self.setupRefresh()
        
    }
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }

  
    private func setupRefresh() {
        refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionView.refreshControl = refreshControl
    }
    
    @objc private func refresh() {
        self.operationsAPI()
        refreshControl.endRefreshing()
    }

}
//MARK:- Networking
extension OperationsVC {
    private func operationsAPI() {
        LottieHelper.shared.startAnimation(view: view)
        print("url === \(URLs.operations)")
        APIs.instance.genericApi(pageNo: 0, url: URLs.operations, method: .get, paameters: nil, headers: nil) { (data:OperationsModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.operationsData = data?.data ?? []
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
        }}else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
//MARK:- Collection Config
extension OperationsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return operationsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeue(indexPath: indexPath) as OperationCell
        
             cell.model = operationsData[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Vibration.selection.vibrate()
        self.selIndx = indexPath.row
        performSegue(withIdentifier: "operationDetailSegue", sender: self)
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "operationDetailSegue" {
            let vc = segue.destination as! OperationDetailVC
            vc.recModel = operationsData[self.selIndx!]
        }
    }
    
    
}
