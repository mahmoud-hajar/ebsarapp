//
//  MyTreatmentsVC.swift
//  Ebsar
//
//  Created by Mohamed on 3/1/21.
//

import UIKit
import FSCalendar
import SwiftyJSON


class MyTreatmentsVC: UIViewController {
    
    @IBOutlet private weak var calender: FSCalendar!
    @IBOutlet private weak var tableView: UITableView!{
        didSet{
            self.tableView.registerNib(cell: MyTreatmentsCell.self)
    }}
    @IBOutlet private weak var drugDayLab: UILabel!{
        didSet{
            self.drugDayLab.text = "medicineDays".localized()
    }}
    
    @IBOutlet private weak var myDrugsLab: UILabel!{
        didSet{
            self.myDrugsLab.text = "myDrugs".localized()
        }
    }
    
    
    
    var Times = [JSON]()
    //var calender:FSCalendar!
    var formatter = DateFormatter()
    var productt:JSON?
    
    var firstDate: Date?
    var lastDate: Date?
    var datesRange: [Date]?
    
    // last date in the range
    // first date in the range
    // var StringDate: Date?
    // let stringDate = "22_11_2020"
    
    var eventsDateArray: [Date] = []
    
    lazy var refreshControl = UIRefreshControl()
    var modelData = [myTreatmentModel]()
    var datesModel = [String]()

    var dateStr = Date().getFormattedDate(format: "YYYY-MM-dd")

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupCalender()
        self.setupRefreshControl()
    
        self.treatmentsAPI(date: self.dateStr)
        self.treatmentDatesAPI()

    }
    
    private func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tableView.refreshControl = refreshControl
    }
    
    @objc private func refresh() {
        self.treatmentsAPI(date: self.dateStr)
        self.treatmentDatesAPI()
        refreshControl.endRefreshing()
    }
    

    
}
//MARK:- Networking
extension MyTreatmentsVC {
    private func treatmentsAPI(date:String) {
        self.modelData.removeAll()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.myTrreatments+"\(date)", method: .get, paameters: nil, headers: nil) { (data:MyTreatmentModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.modelData = data?.data ?? []
                DispatchQueue.main.async {
                    self.tableView.reloadData()
        }}else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
    private func treatmentDatesAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.myDrugs, method: .get, paameters: nil, headers: nil) { (data:MyTreatmentModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.datesModel.removeAll()
                for i  in data?.data ?? [] {
                    self.datesModel.append(i.date ?? "")
                }
                DispatchQueue.main.async {
                    self.calender.reloadData()
                }
            }else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
//MARK:- TableView Configs
extension MyTreatmentsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeue() as MyTreatmentsCell
             
        cell.titleLab.text = modelData[indexPath.row].title
        cell.descriptionLab.text = modelData[indexPath.row].contents
        cell.logo.setImage(from: URLs.mainImageURL + (modelData[indexPath.row].image ?? ""))
        cell.timeLab.text = modelData[indexPath.row].time
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           cell.animateCellFadeInOut()
    }
    
}
//MARK:- Calender Configs
extension MyTreatmentsVC: FSCalendarDelegate,FSCalendarDataSource {
    
    private func setupCalender() {
        calender.scope = .month
        // calender.locale = Locale(identifier: "GMT")
        calender.delegate = self
        calender.dataSource = self
        calender.allowsMultipleSelection = false
        // calender.diy
        // calender.addMotionEffect(<#T##effect: UIMotionEffect##UIMotionEffect#>)
//        calender.appearance.titleFont = UIFont.systemFont(ofSize: 17.0)
//        calender.appearance.headerTitleFont = UIFont.boldSystemFont(ofSize: 18.0)
//        calender.appearance.weekdayFont = UIFont.systemFont(ofSize: 16.0)
//        if #available(iOS 13.0, *) {
//            calender.appearance.todayColor = .systemGroupedBackground
//        } else {
//            // Fallback on earlier versions
//        }
        formatter.dateFormat = "YYYY-MM-dd"
//        eventsDateArray = [ formatter.date(from: "17_11_2020")!]
        for ite in Times{
            eventsDateArray.append(formatter.date(from: ite.stringValue)!)
        }
    }
    
//    func minimumDate(for calendar: FSCalendar) -> Date {
//        return Date()
//    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date().addingTimeInterval((24*60*60)*365)
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
//        if firstDate == nil {
//            firstDate = date
//            datesRange = [firstDate!]
//            print("datesRange contains1: \(datesRange!)")
        self.dateStr = date.getFormattedDate(format: "YYYY-MM-dd")
                //(datesRange?.first?.getFormattedDate(format: "YYYY-MM-dd"))!
            treatmentsAPI(date: dateStr)
//            return
//        }
//        if firstDate != nil && lastDate == nil {
//            if date <= firstDate! {
//                calendar.deselect(firstDate!)
//                firstDate = date
//                datesRange = [firstDate!]
//                datesRange!.removeAll()
//                //print("datesRange contains2: \(datesRange!)")
//                return
//            }
//            let range = datesRange(from: firstDate! , to :date)
//            lastDate = range.last
//            for d in range {
//                if eventsDateArray.contains(d) {
//                    calendar.deselect(d)
//                    calendar.deselect(lastDate!)
//                    datesRange = []
//                    lastDate = nil
//                    let alert = UIAlertController(title: "Error", message: "Cann't Select This Date", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                    break
//              }else {
//                    calendar.select(d)
//            }}
//            datesRange = range
//            datesRange!.removeAll()
//            for itm in range{
//                datesRange!.append(Calendar.current.date(byAdding: .day,value: 1, to: itm)!)
//            }
////            print("datesRange contains3: \(datesRange!)")
////            print("firsssst \(datesRange!.first!)   lasssssst \(datesRange!.last!)")
//            datesRange!.removeAll()
//            return
//        }
//        // both are selected:
//        if firstDate != nil && lastDate != nil {
//            for d in calendar.selectedDates {
//                calendar.deselect(d)
//            }
//            lastDate = nil
//            firstDate = nil
//            datesRange = []
//        }
    }
//    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
//        if firstDate != nil && lastDate != nil  {
//            for d in calendar.selectedDates {
//                calendar.deselect(d)
//            }
//            lastDate = nil
//            firstDate = nil
//            datesRange = []
//            print("datesRange contains6: \(datesRange!)")
//        }
//    }

    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
            if eventsDateArray.contains(date) { return false }
        return true
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        if eventsDateArray.contains(date) { return .systemRed }
        return nil
    }
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if (from > to) { return [Date]() }
        var tempDate = from
        var array = [tempDate]
        while tempDate < to  {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
            
        }
        return array
    }
    


}
//MARK:- FSCalender events
extension MyTreatmentsVC: FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = date.getFormattedDate(format: "yyyy-MM-dd")
        if self.datesModel.contains(dateString) {
            return 1
        }
        return 0
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {

        let dateString = date.getFormattedDate(format: "yyyy-MM-dd")
        if self.datesModel.contains(dateString) {
            return [UIColor(named: "OrangeApp")!]
        }
        return [UIColor.white]
    }
    
}
