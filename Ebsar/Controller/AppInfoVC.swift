//
//  AppInfoVC.swift
//  Ektfa
//
//  Created by Ghoost on 4/14/21.
//

import UIKit
import WebKit

class AppInfoVC: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var backImage: UIImageView!{
        didSet{
            self.backImage.LocalizedImage()
            self.backImage.addActionn(vc: self, action: #selector(dis))
    }}
    
    var isTerms:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.settingAPI()

    }

    @objc private func dis() {
        Vibration.light.vibrate()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func displayUrl(url:String) {
        let urlString = URL(string: url)
        let request = URLRequest(url: urlString!)
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    
}
//MARK:- Networking
extension AppInfoVC {
    private func settingAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.setting, method: .get, paameters: nil, headers: nil) { (data:SettingModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
              if self.isTerms == true {
                    self.displayUrl(url: data?.data?.terms ?? "")
                    self.titleLab.text = "TermsAndConditions".localized()
               }else{
                    self.displayUrl(url: data?.data?.aboutUs ?? "")
                    self.titleLab.text = "AboutTheApp".localized()
            }}else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
}
