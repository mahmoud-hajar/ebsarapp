//
//  SickLeaveRequestVC.swift
//  Ebsar
//
//  Created by Mohamed on 5/2/21.
//
import UIKit

class SickLeaveRequestVC: UIViewController {

    @IBOutlet weak var visualView: UIVisualEffectView!
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
        }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "sickLeaveRequest".localized()
        }}
    @IBOutlet weak var pleaseCompleteLabel: UILabel!{
        didSet{
            self.pleaseCompleteLabel.text = "PleaseCompleteLabel".localized()
        }}
    @IBOutlet weak var fileNameTF: UITextField!{
        didSet{
            self.fileNameTF.placeholder = "fileName".localized()
        }}
    @IBOutlet weak var nameTF: UITextField!{
        didSet{
            self.nameTF.placeholder = "Name";
        }}
    @IBOutlet weak var IDNumberTF: UITextField!{
        didSet{
            self.IDNumberTF.placeholder = "iDNumber".localized()
        }}
    @IBOutlet weak var fileNoTF: UITextField!{
        didSet{
            self.fileNoTF.placeholder = "fileNo".localized()
        }}
    @IBOutlet weak var EmployerTF: UITextField!{
        didSet{
            self.EmployerTF.placeholder = "employer".localized()
        }}
    @IBOutlet weak var WhatIsTheProcedureTF: UITextView!{
        didSet{
            self.WhatIsTheProcedureTF.placeholder = "WhatIsTheProcedure".localized();
        }}
    @IBOutlet weak var sendPress: UIButton!{
        didSet{
            self.sendPress.initButton(title: "send".localized(), titleColor: .white, backgroundColor: #colorLiteral(red: 0.168627451, green: 0.6941176471, blue: 0.6352941176, alpha: 1), roundValue: 5, index: .Medium, fontSize: 16)
        }}
    @IBOutlet weak var downloadBtn: UIButton!{
        didSet{
            self.downloadBtn.initButton(title: "download".localized(), titleColor: .white, backgroundColor: #colorLiteral(red: 0.168627451, green: 0.6941176471, blue: 0.6352941176, alpha: 1), roundValue: 5, index: .Medium, fontSize: 16)
        }}
    @IBOutlet weak var DownloadPress: UIButton!{
        didSet{
            self.DownloadPress.setTitle("download".localized(), for: .normal)
        }}
    @IBOutlet weak var showPress: UIButton!{
        didSet{
            self.showPress.setTitle("show".localized(), for: .normal)
        }}    
    @IBOutlet private weak var uploadView: UIView!
    @IBOutlet private weak var uploadConstrain: NSLayoutConstraint!
    
    
    var recActionTag:Int?
    var recSelectedView:Int?
    var pdfURL:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.visualView.isHidden = true
        
    }

    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionButtons(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            UIView.animate(withDuration: 0.3) {
                self.visualView.isHidden = false
            }
        case 1:
            performSegue(withIdentifier: "pdfSegue", sender: self)
        case 2: // send Buttons
            if validateInputs() {
                let para:[String:Any] = parameters.instance.requestVacationParameters(name:nameTF.text!, hoppy: IDNumberTF.text!, reason: WhatIsTheProcedureTF.text!, file_number: fileNoTF.text!, destination: EmployerTF.text!)
              uploadRequestAPI(para: para)
            }
        case 4:
            guard (fileNameTF.text != nil) else {
                Vibration.error.vibrate()
                return
            }
            self.downloadPdfAPI(uniqueName: self.fileNameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        default:
            break
        }
    }
    
    
   private func checkTag() {
      if self.recSelectedView == 1 {
        if self.recActionTag == 1 {
            self.uploadConstrain.constant = 281
            self.uploadView.isHidden = false
        }
      }else if self.recSelectedView == 1 {
        if self.recActionTag == 2 {
            self.uploadConstrain.constant = 0
            self.uploadView.isHidden = true
        }
    }}
    
    
    
}
//MARK:- Validation
extension SickLeaveRequestVC {
    private func validateInputs() -> Bool {
        guard let name = nameTF.text, !name.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "fieldsIsRequired".localized())
            print("one")
            return false
        }
        guard let fileNo = fileNoTF.text, !fileNo.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "fieldsIsRequired".localized())
            print("two")
            return false
        }
        guard let employer = EmployerTF.text, !employer.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "fieldsIsRequired".localized())
            print("four")
            return false
        }
        guard let id = IDNumberTF.text, !id.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "fieldsIsRequired".localized())
            print("five")
            return false
        }
        guard let procedure = WhatIsTheProcedureTF.text, !procedure.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "fieldsIsRequired".localized())
            print("six")
            return false
        }
      
        return true
    }
    
}

//MARK:- Networking
extension SickLeaveRequestVC {
    private func downloadPdfAPI(uniqueName:String) {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.downloadPdf(uniqueName: uniqueName, pdfReport:self.pdfURL ?? "") { (filePath:String, status:Bool) in
            LottieHelper.shared.hideAnimation()
            if status == true {
                NotificationBannerHelper.shared.showSuccess(title: "success".localized(), subtitle: "")
                UIView.animate(withDuration: 0.3) {
                    self.visualView.isHidden = true
        }}else{
                UIView.animate(withDuration: 0.3) {
                    self.visualView.isHidden = true
    }}}}
    
    private func uploadRequestAPI(para:[String:Any]) {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.requestMultiData(url: URLs.AddNewRequest, image: UIImage(), params: para, header: Headers.getUpdateLogoHeader()) { (data:ErrorModel?, errModel:ErrorModel?, error:Error?) in
            if data?.status == 200 {
                NotificationBannerHelper.shared.showSuccess(title: "success".localized(), subtitle: data?.message ?? "")
                self.navigationController?.popViewController(animated: true)
            }else {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
