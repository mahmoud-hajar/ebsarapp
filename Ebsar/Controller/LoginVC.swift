//
//  LoginVC.swift
//  Ebsar
//
//  Created by Mohamed on 2/18/21.
//

import UIKit
import MOLH
import FlagPhoneNumber

class LoginVC: UIViewController {
    
    var settingData:settingModel?
    @IBOutlet private weak var loginBtn: UIButton!{
        didSet{
            self.loginBtn.initButton(title: "login".localized(), titleColor: .white, backgroundColor: .accentColor, roundValue: 14, index: .Bold, fontSize: 18)
    }}
    @IBOutlet private weak var registerView: UIView!{
        didSet{
            self.registerView.addActionn(vc: self, action: #selector(goToRegister))
    }}
    @IBOutlet weak var userNameTF: FPNTextField!{
        didSet{
            self.userNameTF.placeholder = "phone".localized()
    }}
    @IBOutlet weak var passwordTF: UITextField!{
        didSet{
            self.passwordTF.placeholder = "password".localized()
    }}
    @IBOutlet weak var loginLabel: UILabel!{
        didSet{
            self.loginLabel.text = "login".localized()
    }}
    @IBOutlet weak var creatAccountLabel: UILabel!{
        didSet{
            self.creatAccountLabel.text = "Creataccount".localized()
    }}
    
    @IBOutlet weak var dontHaveAccountLabel: UILabel!{
        didSet{
            self.dontHaveAccountLabel.text = "dontHaveAccount".localized()
    }}
    
    @IBOutlet weak var socialMediaLabel: UILabel!{
        didSet{
            self.socialMediaLabel.text = "socialMediaL".localized()
    }}
    
    var phoneCode:String = "+966"
    var validPhoneNumber = false
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSetting()
        self.preparePhoneTF()

    }
   
    
    
    
    @IBAction private func btnsAction(_ sender: UIButton) {
        sender.springAnimate()
        Vibration.light.vibrate()
        switch sender.tag {
        case 1:
            valideteInput()
        case 2:
            SoicalHelper.shared.openTwitter(twitter: settingData?.twitter ?? "")
        case 3:
            SoicalHelper.shared.openInstagram(instgram: settingData?.instagram ?? "")
        case 4:
            SoicalHelper.shared.openWhatsapp(phone: settingData?.phone1 ?? "")
        case 5:
            SoicalHelper.shared.openFacebook(facebook: settingData?.facebook ?? "")
        default:
            break
        }
    }
    
    @objc private func goToRegister() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        vc.namberForWhatsApp = settingData?.phone1 ?? ""
        present(vc, animated: true, completion: nil)
        
    }
    
    
    
}


//MARK:- Networking
extension LoginVC {
    private func valideteInput() {
        guard let userName = userNameTF.text, !userName.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "userNameIsRequired".localized())
            return
        }
        guard let password = passwordTF.text, !password.isEmpty else {
            Vibration.error.vibrate()
            NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: "passwordIsRequired".localized())
            return
        }
        self.loginAPI()
    }

    private func getSetting() {
        APIs.instance.genericApi(pageNo: 0, url: URLs.settingsURL, method: .get, paameters: nil, headers: nil) { (data:SettingModel?, errorModel:ErrorModel?, error:Error?) in
            if data?.status == 200 {
                if data?.data != nil {
                self.settingData = data?.data
                }
                }else if errorModel != nil {
            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: errorModel?.message ?? "")
            }else {
            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
        }}}
    private func loginAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.loginURL, method: .post, paameters: parameters.instance.loginParameters(phone: userNameTF.text!, password: passwordTF.text!, phone_code: self.phoneCode),  headers: nil) { (data:LoginModel?, errorModel:ErrorModel?, error:Error?) in
            if data?.status == 200 {
                LottieHelper.shared.hideAnimation()
            DispatchQueue.main.async {
                if data?.data != nil {
                    //save data
                    Helper.instance.saveUserToken(token: data?.data?.token ?? "")

                    Helper.instance.saveUserId(Id: String(data?.data?.id ?? 000));
                    
                    Helper.instance.saveUserData(name: (data?.data?.name) ?? "", logo: (data?.data?.logo) ?? "", phone: (data?.data?.phone) ?? "", email: (data?.data?.email) ?? "", address: (data?.data?.address) ?? ""
                    )
                    MOLH.reset()
                }

                
            }}else if errorModel != nil {
                NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: errorModel?.message ?? "")
            }else {
                NotificationBannerHelper.shared.showError(title: "Error".localized(), subtitle: data?.message ?? "")
       }}}
    
    
    
}
extension LoginVC:FPNTextFieldDelegate {
    
    private func preparePhoneTF() {
        self.userNameTF.setFlag(countryCode: FPNCountryCode.SA)
        userNameTF.delegate = self
    }
    
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        present(navigationViewController, animated: true, completion: nil)
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
       // let x = dialCode.replacingOccurrences(of: "+", with: "00")
        phoneCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            validPhoneNumber = false
            self.userNameTF.text = textField.getRawPhoneNumber()
            
        } else {
            validPhoneNumber = false
            // Do something...
        }
    }
    
    
}
