//
//  OperationDetailVC.swift
//  Ebsar
//
//  Created by mahmoud hajar on 30/12/2021.
//

import UIKit
import youtube_ios_player_helper
import MMPlayerView
import AVFoundation

struct VideoModel {
    let url:String?
    let title:String?
    let content:String?
    let isExternal:Bool?
    let isYoutube:Bool?
}
class OperationDetailVC: UIViewController {
    
    @IBOutlet private weak var headTitleLab: UILabel!
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet private weak var tableView: UITableView!

    var offsetObservation: NSKeyValueObservation?
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        l.coverFitType = .fitToPlayerView
        l.videoGravity = AVLayerVideoGravity.resizeAspect
        l.replace(cover: CoverA.instantiateFromNib())
        l.repeatWhenEnd = true
        return l
    }()
    
    
    
    
    var recModel:operationsModel?
    private var videosData:[VideoModel] = []


    override func viewDidLoad() {
        super.viewDidLoad()

        self.setData()
        self.tableViewConfigs()
       // self.playerConfigs()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }
    
    deinit {
        offsetObservation?.invalidate()
        offsetObservation = nil
        print("ViewController deinit")
    }
    
    
    

}
//MARK: - PlayerView {
extension OperationDetailVC {
    
    func playVideo(cell:UITableViewCell,row:Int) {
            guard let cell = cell as? VideoCell else { return }
        if let videoURL = URL(string:URLs.mainImageURL + "\(self.videosData[row].url ?? "")") {
                let player = AVPlayer(url: videoURL)
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = cell.videoView.bounds
                playerLayer.name = "Video"
                cell.videoView.layer.addSublayer(playerLayer)
                player.play()
    }}

        func removeVideo(cell:UITableViewCell,row:Int) {
            guard let cell = cell as? VideoCell, let layers = cell.videoView.layer.sublayers else {
                return
            }
            for layer in layers {
                if layer.name == "Video" {
                    //(layer as? AVPlayerLayer)?.player?.pause()
                    layer.removeFromSuperlayer()
                }
            }
        }
    
    
}

//MARK: - UI
extension OperationDetailVC {
    
    private func setData() {
        self.videosData.removeAll()
        
        self.headTitleLab.text = recModel?.title
        let youtubeVideo = recModel?.youtubeVideo ?? ""
        let twitterVideo = recModel?.twitterVideo ?? ""
        
        if youtubeVideo.utf8.count > 2 {
            self.videosData.append(VideoModel(url: youtubeVideo, title: recModel?.youtubeTitle ?? "", content: recModel?.contents ?? "", isExternal: true,isYoutube: true))
        }
        if twitterVideo.utf8.count > 2 {
            self.videosData.append(VideoModel(url: twitterVideo, title: recModel?.twitterTitle ?? "", content: recModel?.contents ?? "", isExternal: true,isYoutube: false))
        }
        if !(self.recModel?.video?.isEmpty ?? true) {
            self.videosData.append(VideoModel(url: twitterVideo, title: recModel?.title ?? "", content: recModel?.contents ?? "", isExternal: false,isYoutube: false))
        }
        for i in recModel?.operationVideoFk ?? [] {
            if i.video?.utf8.count ?? 1 > 2 {
                self.videosData.append(VideoModel(url: i.video, title: recModel?.title ?? "", content: recModel?.contents ?? "", isExternal: false,isYoutube: false))
            }
            if i.youtubeVideo?.utf8.count ?? 1 > 2 {
                self.videosData.append(VideoModel(url: i.youtubeVideo, title: recModel?.youtubeTitle ?? "", content: recModel?.contents ?? "", isExternal: true,isYoutube: true))
            }
            if i.twitterVideo?.utf8.count ?? 1 > 2 {
                self.videosData.append(VideoModel(url: i.twitterVideo, title: recModel?.twitterVideo ?? "", content: recModel?.contents ?? "", isExternal: true,isYoutube: false))
        }}
        DispatchQueue.main.async {
            print("data count === \(self.videosData.count)")
            self.tableView.reloadData()
        }
    }
    
    
    
}

//MARK: - TableView Configs
extension OperationDetailVC:UITableViewDelegate, UITableViewDataSource, YTPlayerViewDelegate,WKUIDelegate  {
    
    private func tableViewConfigs() {
        self.tableView.registerNib(cell: WebViewCell.self)
        self.tableView.registerNib(cell: VideoCell.self)
        self.tableView.registerNib(cell: YoutubeCell.self)
        self.tableView.isPagingEnabled = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.videosData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //self.currIndexPath = indexPath
        if self.videosData[indexPath.row].isExternal == true &&  self.videosData[indexPath.row].isYoutube == false {
            let cell = self.tableView.dequeue() as WebViewCell
                cell.title.text = self.videosData[indexPath.row].title
                cell.subTitle.text = self.videosData[indexPath.row].content
                cell.webVeiew.uiDelegate = self
                let myRequest = URLRequest(url: URL(string:(self.videosData[indexPath.row].url ?? ""))!)
                cell.webVeiew.load(myRequest)
            return cell
        }else if self.videosData[indexPath.row].isExternal == true &&  self.videosData[indexPath.row].isYoutube == true {
            let cell = self.tableView.dequeue() as YoutubeCell
                cell.title.text = self.videosData[indexPath.row].title
                cell.subTitle.text = self.videosData[indexPath.row].content
                cell.videoView.delegate = self
                let id = self.extractId(url: self.videosData[indexPath.row].url ?? "")
                cell.videoView.load(withVideoId: id)
                cell.videoView.playVideo()
            return cell
        } else {
            let cell = self.tableView.dequeue() as VideoCell
                cell.model = self.videosData[indexPath.item]
            
            let url = URLs.mainImageURL + (self.videosData[indexPath.row].url ?? "")
            playVideo(cell: cell, row: indexPath.row)
  
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addTarget(self, action: #selector(self.changeVideoState(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if  self.videosData[indexPath.row].isExternal == true &&  self.videosData[indexPath.row].isYoutube == true  {
            guard let cell = cell as? YoutubeCell else { return }
            let visibleCells = tableView.visibleCells
            let minIndex = visibleCells.startIndex
            if tableView.visibleCells.firstIndex(of: cell) == minIndex {
               // cell.videoView.playVideo()
              
          }}else if self.videosData[indexPath.row].isExternal == false &&  self.videosData[indexPath.row].isYoutube == false {
           guard let cell = self.tableView.cellForRow(at: indexPath) as? VideoCell else { return }
              let visibleCells = tableView.visibleCells
              let minIndex = visibleCells.startIndex
              if tableView.visibleCells.firstIndex(of: cell) == minIndex {
                  playVideo(cell: cell, row: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if  self.videosData[indexPath.row].isExternal == true &&  self.videosData[indexPath.row].isYoutube == true  {
            guard let cell = cell as? YoutubeCell else { return }
               cell.videoView.stopVideo()
        }else if self.videosData[indexPath.row].isExternal == false &&  self.videosData[indexPath.row].isYoutube == false {
            self.removeVideo(cell: cell, row: indexPath.row)
            
    }}
    
    private func extractId(url:String) -> String {
        var id = ""
         let videoID = url.components(separatedBy: "=").last ?? ""
            id = videoID
        return id
    }
    
    @objc private func changeVideoState(_ sender: UIButton) {
        let row = sender.tag
        let indexPath = IndexPath(row: row, section: 0)
        let cell = self.tableView.cellForRow(at: indexPath) as! VideoCell
        if cell.btnPlay.currentImage == UIImage(named: "play") {
            self.playVideo(cell: cell, row: row)
        }else if cell.btnPlay.currentImage == UIImage(named: "pause") {
            self.removeVideo(cell: cell, row: row)
        }
    }
    
}
