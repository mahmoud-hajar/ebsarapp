//
//  VerficationCodeVC.swift
//  Ebsar
//
//  Created by Mohamed on 4/27/21.
//

import UIKit
import FirebaseAuth
import MOLH

class VerficationCodeVC: UIViewController {
    var timer:Timer?
    var startTime = 60.0
    
     
    var recPhoneCode = ""
    var birthday = ""
    var gender = ""
    var password = ""
    var name = ""
    var phone = ""
    var code = ""
    var userImage = UIImage()
    let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
    var verificationId = ""
    
    //label
    @IBOutlet weak var activationCodeLabel: UILabel!{
        didSet{
            self.activationCodeLabel.text = "activationCode".localized()
        }}
    @IBOutlet weak var pleasEnterLabel: UILabel!{
        didSet{
            self.pleasEnterLabel.text = "pleasEnter..".localized()
        }}
    @IBOutlet weak var phoneLabel: UILabel!{
        didSet{
            self.phoneLabel.text = phone
        }
    }
    @IBOutlet weak var resendTheCodeLabel: UILabel!{
        didSet{
            self.resendTheCodeLabel.text = "resendTheCode".localized()
        }}
    // Text Field

    @IBOutlet weak var verificationcodeTF: UITextField!
    //buttons
    
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var confirmationBButton: UIButton!{
        didSet{
            self.confirmationBButton.initButton(title: "confirmation".localized(), titleColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), backgroundColor: #colorLiteral(red: 0.2235294118, green: 0.6941176471, blue: 0.631372549, alpha: 1), roundValue: 5, index: .Bold, fontSize: 16)
        }}
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
           timeButton.isUserInteractionEnabled = false
           timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        verificationcodeTF.textContentType = .oneTimeCode
        verificationcodeTF.becomeFirstResponder()
        self.validatePhoneAPI()
        
    }
    
    @IBAction func ActionButtons(_ sender: UIButton) {
        switch sender.tag {
        case 0:// time Button for resend code
          //  setPhone()
            self.validatePhoneAPI()
        case 1:// confirmation Button
            let verificationCode  = verificationcodeTF.text!
            if verificationCode.isEmpty {
                self.showNoti("enterVerificationCode".localized())
            }else if verificationCode.count != 6 {
                self.showNoti("invalidVerificationCode".localized())
            }else {
                LottieHelper.shared.startAnimation(view: view)
                  self.VerificationAPI(completion: { (status) in
                      let param = parameters.instance.registerParameters(name:  self.name, phone_code: self.recPhoneCode, password:  self.password, phone:  self.phone, gender:  self.gender, birthday: self.birthday, software_type: "ios")
                self.registerAPI(param: param, img: self.userImage)
        })}
        default:
            break
    }}
    
 
    
}
//MARK: - Networking
extension VerficationCodeVC {
    
    private func validatePhoneAPI() {
        let userPhone = self.recPhoneCode + self.phone
        PhoneAuthProvider.provider().verifyPhoneNumber(userPhone, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                Vibration.error.vibrate()
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: error.localizedDescription)
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            self.verificationId = verificationID ?? ""
           // print("verfication id === \(self.verificationId)")
            self.showNoti("we_sent_to_you_an_sms_verification_code".localized())
   }}
    
    private func VerificationAPI(completion: @escaping (_ status: Bool) -> ()) {
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationId ,
            verificationCode: self.verificationcodeTF.text ?? "")
        Auth.auth().signIn(with: credential) { (authResult, error) in
            LottieHelper.shared.hideAnimation()
            if let error = error {
                Vibration.error.vibrate()
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: error.localizedDescription)
                completion(false)
                return
            }
            completion(true)
    }}
    
    func registerAPI(param:[String:Any],img:UIImage) {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.requestMultiData(url: URLs.registerURL, image: img, params: param, header: nil) { (data:LoginModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                if data?.data != nil {
                    //save data
                    Helper.instance.saveUserToken(token: data?.data?.token ?? "")

                    Helper.instance.saveUserId(Id: String(data?.data?.id ?? 000));
                    
                    Helper.instance.saveUserData(name: (data?.data?.name) ?? "", logo: (data?.data?.logo) ?? "", phone: (data?.data?.phone) ?? "", email: (data?.data?.email) ?? "", address: (data?.data?.address) ?? ""
                    )
                    
//                    let vc = UIStoryboard(name: "Main", bundle: nil)
//                    let rootVc = vc.instantiateViewController(withIdentifier: "HomeID")
//                    self.present(rootVc, animated: true, completion: nil)
                          
                    MOLH.reset()

                }
               // completion(true)
            }else if data?.status == 402 {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
                //completion(false)
            } else if errModel != nil {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: errModel?.message ?? "")
                //completion(false)
            }else {
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
                //completion(false)
   }}}
    
    
}
//MARK:- Selectors
extension VerficationCodeVC {
    @objc func onTimerFires(){
        timeButton.isUserInteractionEnabled = false
        startTime -= 1.0
        timeButton.setTitle("00 : \(Int(startTime))", for: .normal)
        if startTime <= 0.0  {
            timeButton.setTitle("Send".localized(), for: .normal)
            timeButton.isUserInteractionEnabled = true
            if let tt = timer {
                tt.invalidate()
            }
            timer = nil
            
    }}
}


// self.registerAPI(para: para, img: self.userImage)

//    let credential = PhoneAuthProvider.provider().credential(
//        withVerificationID: verificationID ?? "",
//        verificationCode: verificationCode)
//    Auth.auth().signIn(with: credential) { (authResult, error) in
//        LottieHelper.shared.hideAnimation()
//        if let error = error {
//            Vibration.error.vibrate()
//            NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: error.localizedDescription)
//            return
//        }
//}
//    func setPhone(){
//        let phoneNumber = code + phone
//        LottieHelper.shared.startAnimation(view: view)
//        print("phone \(phoneNumber)")
//        Auth.auth().settings!.isAppVerificationDisabledForTesting = false
//        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
//            LottieHelper.shared.hideAnimation()
//            if let error = error {
//                self.showNoti_without_Localization(error.localizedDescription)
//                return
//            }
//            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
//            self.showNoti("we_sent_to_you_an_sms_verification_code".localized())
//        }
//    }
