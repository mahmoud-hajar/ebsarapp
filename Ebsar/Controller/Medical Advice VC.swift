
import UIKit

class Medical_Advice_VC: UIViewController {
    
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "medicalAdvice".localized()
    }}
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.registerCell(cellClass: MedicalAdviceCollectionCell.self)
    }}
    @IBOutlet weak var imagee: UIImageView!
    @IBOutlet weak var Maintitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var medicalData = [medicalAdviceModel]()
    var indx:Int = 0
    
    lazy var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        medicalAPI()
        setupRefresh()
    }
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }
    
    private func setData(model:medicalAdviceModel?) {
        self.Maintitle.text = model?.title ?? ""
        self.textView.text = model?.contents ?? ""
        self.imagee.setImage(from: URLs.mainImageURL+(model?.image ?? ""))
    }
    
    private func setupRefresh() {
        refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.collectionView.refreshControl = refreshControl
    }
    
    @objc private func refresh() {
        self.medicalAPI()
        refreshControl.endRefreshing()
    }
    
    
}
//MARK:- Networking
extension Medical_Advice_VC {
    private func medicalAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.medical_advices, method: .get, paameters: nil, headers: nil) { (data:MedicalAdviceModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.medicalData = data?.data ?? []
                DispatchQueue.main.async {
                    self.setData(model: data?.data?.first)
                    self.collectionView.reloadData()
        }}else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
// MARK:- CollectionView
extension Medical_Advice_VC: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medicalData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeue(indexPath: indexPath) as MedicalAdviceCollectionCell
           cell.text.text = self.medicalData[indexPath.row].title
        if self.indx == indexPath.row {
            cell.mainView.backgroundColor = UIColor(named: "GreenApp")
            cell.text.textColor = .white
        }else{
            cell.mainView.backgroundColor = .white
            cell.text.textColor = .black
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Vibration.light.vibrate()
             self.indx = indexPath.row
             self.setData(model: medicalData[indexPath.row])
    }
    
    
    
}
extension Medical_Advice_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: title?.size(withAttributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)]).width ?? 0 + 50, height: self.collectionView.frame.height - 5)
      }
}
