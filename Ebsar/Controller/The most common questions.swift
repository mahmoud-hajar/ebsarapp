//
//  The most common questions.swift
//  Ebsar
//
//  Created by Mohamed on 2/22/21.
//

import UIKit

class The_most_common_questions: UIViewController {
    
    @IBOutlet private weak var imageBack: UIImageView!{
        didSet{
            self.imageBack.LocalizedImage()
            self.imageBack.addActionn(vc: self, action: #selector(disAction))
    }}
    @IBOutlet private weak var titlee: UILabel!{
        didSet{
            self.titlee.text = "TheMostCommonQuestions".localized()
    }}
    @IBOutlet private weak var tabelView: UITableView!{
        didSet{
            self.tabelView.registerNib(cell: ElmahzuratTabelCell.self)
    }}
    
    lazy var refreshControl = UIRefreshControl()
    var questationsData = [questaionsModel]()
    
    var rowHeight = 47
    
    var expandedIndexPath: IndexPath?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.questaionsAPI()
        
    }
    
    @objc private func disAction() {
        Vibration.light.vibrate()
        navigationController?.popViewController(animated: true)
    }

  
    private func setupRefresh() {
        refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.tabelView.refreshControl = refreshControl
    }
    
    @objc private func refresh() {
        self.questaionsAPI()
        refreshControl.endRefreshing()
    }

}

//MARK:- Networking
extension The_most_common_questions {
    private func questaionsAPI() {
        LottieHelper.shared.startAnimation(view: view)
        APIs.instance.genericApi(pageNo: 0, url: URLs.questions, method: .get, paameters: nil, headers: nil) { (data:QuestaionsModel?, errModel:ErrorModel?, error:Error?) in
            LottieHelper.shared.hideAnimation()
            if data?.status == 200 {
                self.questationsData = data?.data ?? []
                DispatchQueue.main.async {
                    self.tabelView.reloadData()
        }}else{
                NotificationBannerHelper.shared.showError(title: "error".localized(), subtitle: data?.message ?? "")
    }}}
    
}
//MARK:- TablevView Config
extension The_most_common_questions: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questationsData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tabelView.dequeue() as ElmahzuratTabelCell
    
            cell.model = questationsData[indexPath.item]
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
              cell.animateCellFadeInOut()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Vibration.selection.vibrate()
        self.expandedIndexPath = indexPath
        let cell = self.tabelView.cellForRow(at: indexPath) as! ElmahzuratTabelCell
        self.rowHeight = Int(cell.answerLabel.estimatedHeightOfLabel()) + 90
        UIView.transition(with: tableView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.tabelView.beginUpdates()
            self.tabelView.endUpdates()
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.expandedIndexPath == indexPath {
            return CGFloat(self.rowHeight)
        }else{
            return 47
    }}
    
    
}
