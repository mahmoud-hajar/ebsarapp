//
//  SettingsModel.swift
//  Ebsar
//
//  Created by Mohamed on 4/25/21.
///

import Foundation

// MARK: - SettingModel
struct SettingModel: Codable {
    let data: settingModel?
    let message: String?
    let status: Int?
}

// MARK: - DataClass
struct settingModel: Codable {
    let id: Int?
    let headerLogo, footerLogo, loginBanner, imageSlider: String?
    let title, desc, footerDesc, address1: String?
    let address2, phone1, phone2, email1: String?
    let link: String?
    let latitude, longitude: Double?
    let address: String?
    let offerNotification: Int?
    let facebook, twitter, instagram, linkedin: String?
    let telegram, youtube, googlePlus, snapchatGhost: String?
    let whatsapp, aboutApp, arTermisCondition, enTermisCondition: String?
    let terms:String?
    let aboutUs:String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case headerLogo = "header_logo"
        case footerLogo = "footer_logo"
        case loginBanner = "login_banner"
        case imageSlider = "image_slider"
        case title, desc
        case footerDesc = "footer_desc"
        case address1, address2, phone1, phone2, email1, link, latitude, longitude, address
        case offerNotification = "offer_notification"
        case facebook, twitter, instagram, linkedin, telegram, youtube
        case googlePlus = "google_plus"
        case snapchatGhost = "snapchat_ghost"
        case whatsapp
        case aboutApp = "about_app"
        case arTermisCondition = "ar_termis_condition"
        case enTermisCondition = "en_termis_condition"
        case terms
        case aboutUs = "about_us"
    }
}
