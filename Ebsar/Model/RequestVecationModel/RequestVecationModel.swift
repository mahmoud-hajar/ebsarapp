//
//  RequestVecationModel.swift
//  Ebsar
//
//  Created by Ghoost on 5/11/21.
//

import Foundation

// MARK: - RequestVecationModel
struct RequestVecationModel: Codable {
    let vacationReport: ExamineReport?
   // let vacationList: [ExamineReport]?
    let medicalReport: ExamineReport?
  //  let medicalList: [ExamineReport]?
    let examineReport: ExamineReport?
   // let examineList: [ExamineReport]?
    let glassesMeasurementReport: ExamineReport?
 //   let glassesMeasurementList: [ExamineReport]?
    let message: String?
    let status: Int?

    enum CodingKeys: String, CodingKey {
        case vacationReport = "vacation_report"
      //  case vacationList = "vacation_list"
        case medicalReport = "medical_report"
       // case medicalList = "medical_list"
        case examineReport = "examine_report"
       // case examineList = "examine_list"
        case glassesMeasurementReport = "glasses_measurement_report"
        //case glassesMeasurementList = "glasses_measurement_list"
        case message, status
    }
}

// MARK: - ExamineReport
struct ExamineReport: Codable {
    let id, userID: Int?
    let pdf: String?
    let type: TypeEnum?
    let createdAt, updatedAt: String?
    let requestFk: RequestFk?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case pdf, type
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case requestFk = "request_fk"
    }
}


enum TypeEnum: String, Codable {
    case examine = "examine"
    case glassesMeasurement = "glasses_measurement"
    case medical = "medical"
    case vacation = "vacation"
}
