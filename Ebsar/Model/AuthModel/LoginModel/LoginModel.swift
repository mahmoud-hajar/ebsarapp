
import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let data: loginModel?
    let message: String?
    let status: Int?
}

// MARK: - DataClass
struct loginModel: Codable {
    let id: Int?
    let code, userType, phoneCode, phone: String?
    let name, email, address, gender: String?
    let birthday, image, logo, banner: String?
    let approvedStatus, softwareType, token: String?

    enum CodingKeys: String, CodingKey {
        case id, code
        case userType = "user_type"
        case phoneCode = "phone_code"
        case phone, name, email, address, gender, birthday, image, logo, banner
        case approvedStatus = "approved_status"
        case softwareType = "software_type"
        case token
    }
}
// MARK: - LoginModel
struct ErrorModel: Codable {
    let message: String?
    let status: Int?
}

