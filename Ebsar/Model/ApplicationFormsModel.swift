import Foundation

// MARK: - ApplicationForms
struct ApplicationForms: Codable {
    let vacationReport, medicalReport, examineReport, glassesMeasurementReport: Report?
    let message: String?
    let status: Int?

    enum CodingKeys: String, CodingKey {
        case vacationReport = "vacation_report"
        case medicalReport = "medical_report"
        case examineReport = "examine_report"
        case glassesMeasurementReport = "glasses_measurement_report"
        case message, status
    }
}

// MARK: - Report
struct Report: Codable {
    let id, userID: Int?
    let pdf, type: String?
    let requestFk: RequestFk?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case pdf, type
        case requestFk = "request_fk"
    }
}

// MARK: - RequestFk
struct RequestFk: Codable {
    let id, requestID: Int?
    let fullName: String?
    let hoppy: Hoppy?
    let reason: String?
    let fileNumber: Int?
    let destination: String?

    enum CodingKeys: String, CodingKey {
        case id
        case requestID = "request_id"
        case fullName = "full_name"
        case hoppy, reason
        case fileNumber = "file_number"
        case destination
    }
}

enum Hoppy: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Hoppy.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Hoppy"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}
