//
//  notificationModel.swift
//  Ebsar
//
//  Created by Mohamed on 5/2/21.
//

import Foundation

// MARK: - NotificationModel
struct NotificationModel: Codable {
    let data: [notificationModel]?
    let message: String?
    let status: Int?
}

// MARK: - Datum
struct notificationModel: Codable {
    let id, notificationInfoID, fromUserID, toUserID: Int?
    let title, message, image, notificationDate: String?
    let actionType: String?
    let notiType:String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case notificationInfoID = "notification_info_id"
        case fromUserID = "from_user_id"
        case toUserID = "to_user_id"
        case title, message, image
        case notificationDate = "notification_date"
        case actionType = "action_type"
        case notiType = "noti_type"
    }
}
