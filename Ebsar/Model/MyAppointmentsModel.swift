//
//  MyAppointmentsModel.swift
//  Ebsar
//
//  Created by Mohamed on 4/27/21.
//

import Foundation

// MARK: - MyAppointmentsModel
struct MyAppointmentsModel: Codable {
    let data: [myAppointmentsModel]?
    let message: String?
    let status: Int?
}

// MARK: - Datum
struct myAppointmentsModel: Codable {
    let id, userID: Int?
    let title, date, time: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case title, date, time
    }
}
