//
//  MyTreatmentModel.swift
//  Ebsar
//
//  Created by Ghoost on 5/12/21.
//

import Foundation

struct MyTreatmentModel: Codable {
    let data: [myTreatmentModel]?
    let message: String?
    let status: Int?
}

// MARK: - Datum
struct myTreatmentModel: Codable {
    let id, userID: Int?
    let title, date, time, image: String?
    let contents, createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case title, date, time, image, contents
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
