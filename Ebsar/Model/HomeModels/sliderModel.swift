
import Foundation

// MARK: - SliderModel
struct SliderModel: Codable {
    let data: [sliderModel]?
    let message: String?
    let status: Int?
}

// MARK: - Datum
struct sliderModel: Codable {
    let id: Int?
    let title, image: String?
}
