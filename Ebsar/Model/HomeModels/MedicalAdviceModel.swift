//
//  MedicalAdviceModel.swift
//  Ebsar
//
//  Created by Ghoost on 5/16/21.
//

import Foundation

struct MedicalAdviceModel: Codable {
    let data: [medicalAdviceModel]?
    let message: String?
    let status: Int?
}

// MARK: - Datum
struct medicalAdviceModel: Codable {
    let id: Int?
    let title, image, contents: String?
}
