//
//  OperationModel.swift
//  Ebsar
//
//  Created by Ghoost on 5/16/21.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let operationsModel = try? newJSONDecoder().decode(OperationsModel.self, from: jsonData)

import Foundation

// MARK: - OperationsModel
struct OperationsModel: Codable {
    let data: [operationsModel]?
    let status: Int?
    let message: String?
}

// MARK: - Datum
struct operationsModel: Codable {
    let contents, updatedAt, createdAt: String?
    let youtubeVideo: String?
    let image: String?
    let operationVideoFk: [OperationVideoFk]?
    let title, type, videoTitle, youtubeTitle: String?
    let video, twitterTitle: String?
    let twitterVideo: String?
    let userID, id: Int?

    enum CodingKeys: String, CodingKey {
        case contents
        case updatedAt = "updated_at"
        case createdAt = "created_at"
        case youtubeVideo = "youtube_video"
        case image
        case operationVideoFk = "operation_video_fk"
        case title, type
        case videoTitle = "video_title"
        case youtubeTitle = "youtube_title"
        case video
        case twitterTitle = "twitter_title"
        case twitterVideo = "twitter_video"
        case userID = "user_id"
        case id
    }
}

// MARK: - OperationVideoFk
struct OperationVideoFk: Codable {
    let id: Int?
    let videoTitle, video, youtubeTitle: String?
    let youtubeVideo: String?
    let twitterTitle: String?
    let twitterVideo: String?
    let operationID: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case videoTitle = "video_title"
        case video
        case youtubeTitle = "youtube_title"
        case youtubeVideo = "youtube_video"
        case twitterTitle = "twitter_title"
        case twitterVideo = "twitter_video"
        case operationID = "operation_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

