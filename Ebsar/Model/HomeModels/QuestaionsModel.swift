//
//  QuestaionsModel.swift
//  Ebsar
//
//  Created by Ghoost on 5/16/21.
//

import Foundation

// MARK: - QuestaionsModel
struct QuestaionsModel: Codable {
    let data: [questaionsModel]?
    let message: String?
    let status: Int?
}

// MARK: - Datum
struct questaionsModel: Codable {
    let id: Int?
    let question, answer: String?

    enum CodingKeys: String, CodingKey {
        case id, question, answer
    }
}
