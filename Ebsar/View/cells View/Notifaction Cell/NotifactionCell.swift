//
//  NotifactionCell.swift
//  Ebsar
//
//  Created by Mohamed on 2/28/21.
//

import UIKit

class NotifactionCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageNotifaction: UIImageView!
    
    
    var notiModel: notificationModel? {
        didSet{
//            self.dateLabel.text = notiModel?.
            self.titleLabel.text = notiModel?.title
            self.contentLabel.text = notiModel?.message
    
        }
    }
    
    
    
}
