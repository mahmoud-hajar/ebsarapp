

import UIKit

class MyAppointmentsTabelCell: UITableViewCell {

    @IBOutlet weak var titleText: UILabel!{
        didSet{
            self.titleText.text = "OperationDate".localized()
        }
    }
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var model:myAppointmentsModel?{
        didSet{
            self.dateLabel.text = model?.date
            self.timeLabel.text = model?.time
            self.titleText.text = model?.title
    }}

}
