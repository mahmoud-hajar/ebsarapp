//
//  ElmahzuratTabelCell.swift
//  Ebsar
//
//  Created by Mohamed on 2/22/21.
//

import UIKit

class ElmahzuratTabelCell: UITableViewCell {

    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var questionView: DesignView!
    @IBOutlet weak var questionLabel: NSLayoutConstraint!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var questaionLab: UILabel!
    @IBOutlet weak var anserConstrain: NSLayoutConstraint!

    var model:questaionsModel? {
        didSet{
            self.questaionLab.text = model?.question
            self.answerLabel.text = model?.answer
    }}
    
}
