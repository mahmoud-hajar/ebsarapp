//
//  MyTreatmentsCell.swift
//  Ebsar
//
//  Created by Ghoost on 5/12/21.
//

import UIKit

class MyTreatmentsCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var descriptionLab: UILabel!
    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var logo: UIImageView!{
        didSet{
            self.logo.setRoundCorners(10.0)
    }}
    
    
}
