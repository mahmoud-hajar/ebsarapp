//
//  WebViewCell.swift
//  Ebsar
//
//  Created by mahmoud hajar on 15/12/2021.
//

import UIKit
import WebKit

class WebViewCell: UITableViewCell {
    
    @IBOutlet weak var webVeiew: WKWebView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var contentBg: UIView!{
        didSet{
            self.contentBg.backgroundColor = .black.withAlphaComponent(0.5)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
