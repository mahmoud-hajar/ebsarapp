//
//  YoutubeCell.swift
//  Ebsar
//
//  Created by mahmoud hajar on 15/12/2021.
//

import UIKit
import youtube_ios_player_helper

class YoutubeCell: UITableViewCell {
    
    @IBOutlet weak var videoView: YTPlayerView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet private weak var contentBg: UIView!{
        didSet{
            self.contentBg.backgroundColor = .black.withAlphaComponent(0.5)
    }}

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.videoView.addSubview(self.contentBg)
        
    }
    
    
    
}
