//
//  HomeCollectionCell.swift
//  Ebsar
//
//  Created by Mohamed on 2/22/21.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
