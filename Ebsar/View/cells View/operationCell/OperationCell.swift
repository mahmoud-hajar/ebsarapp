//
//  OperationCell.swift
//  Ebsar
//
//  Created by Ghoost on 5/16/21.
//

import UIKit

class OperationCell: UICollectionViewCell {

    @IBOutlet weak var logo: UIImageView!{
        didSet{
            self.logo.setRoundCorners(10)
    }}
    
    @IBOutlet weak var titleLab: UILabel!
    
    
    var model:operationsModel? {
        didSet{
            self.logo.setImage(from: URLs.mainImageURL+(model?.image ?? ""))
            self.titleLab.text = model?.title
    }}
    
}
