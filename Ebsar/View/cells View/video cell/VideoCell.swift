//
//  VideoCell.swift
//  Ebsar
//
//  Created by mahmoud hajar on 15/12/2021.
//

import UIKit
import AVKit
import AVFoundation

class VideoCell: UITableViewCell {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet private weak var contentBg: UIView!{
        didSet{
            self.contentBg.backgroundColor = .black.withAlphaComponent(0.5)
    }}
    @IBOutlet weak var btnPlay: UIButton!{
        didSet{
            self.btnPlay.isHidden = true
        }
    }
    @IBOutlet weak var slider: UISlider!{
        didSet{
            self.slider.isHidden = true
        }
    }

    
    var player = AVPlayer()
    var playerLayer = AVPlayerLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    
        self.videoView.addSubview(btnPlay)
        
    }
    
    
    var model:VideoModel? {
        didSet{
            self.title.text = self.model?.title
            self.subTitle.text = self.model?.content
        if let videoURL = URL(string:URLs.mainImageURL + "\(self.model?.url ?? "")")  {
//            self.player = AVPlayer(url: videoURL)
//            self.playerLayer = AVPlayerLayer(player: player)
//            playerLayer.frame = self.videoView.bounds
//            self.layer.addSublayer(playerLayer)
//            self.videoView.layer.addSublayer(playerLayer)
         //   player.play()
    }}}
    
    
}
class PlayerVideoView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self;
    }

    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer;
    }
    
    var player: AVPlayer? {
        get {
            return playerLayer.player;
        }
        set {
            playerLayer.player = newValue;
        }
    }
}
