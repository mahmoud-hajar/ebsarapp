//
//  Helper.swift
//  Ebsar
//
//  Created by Mohamed on 2/18/21.
//

import UIKit
import FTNotificationIndicator
import CoreLocation
import MapKit

class Helper: NSObject {
    private override init() {}
    static let instance = Helper()
    private let def = UserDefaults.standard
    // Saving data
    func saveFirebaseToken(token: String) {
        def.setValue(token, forKey: "FCMToken")
        def.synchronize()
    }
    func saveUserToken(token:String) {
        let def = UserDefaults.standard
        def.setValue(token, forKey: "token")
        def.synchronize()
    }
    // Check data
    func checkUserToken() ->Bool{
        let def = UserDefaults.standard
        return (def.object(forKey: "token") as? String) != nil
    }
    // Get data
    func getUserToken()->String{
        let def = UserDefaults.standard
        return def.object(forKey: "token") as! String
    }
    // Saving id data
    func saveUserId(Id:String) {
        let def = UserDefaults.standard
        def.setValue(Id, forKey: "id")
        def.synchronize()
    }
    // Check id data
    func checkUserId() ->Bool{
        let def = UserDefaults.standard
        return (def.object(forKey: "id") as? String) != nil
    }
    // Get id data
    func getUserId()->String{
        let def = UserDefaults.standard
        return def.object(forKey: "id") as! String
    }
    func getFirebaseToken() -> String {
        return (def.object(forKey: "FCMToken") as! String)
    }
    
    func saveUserData(name:String,logo:String,phone:String,email:String,address:String) {
        def.setValue(name, forKey: "name")
        def.setValue(logo, forKey: "logo")
        def.setValue(phone, forKey: "phone")
        def.setValue(email, forKey: "email")
        def.setValue(address, forKey: "address")
        def.synchronize()
    }
    
    func showNoti(_ txt:String, appName:String){
        FTNotificationIndicator.setNotificationIndicatorStyle(.light)
        FTNotificationIndicator.showNotification(with: UIImage(named: "logo"), title: appName.localized(), message: txt.localized())
        FTNotificationIndicator.setDefaultDismissTime(4.0)
    }
    
    
    func deletUserDefaults() {
       def.removeObject(forKey: "phone")
       def.removeObject(forKey: "name")
       def.removeObject(forKey: "email")
       def.removeObject(forKey: "token")
       def.removeObject(forKey: "id")
       def.removeObject(forKey: "userType")
       def.removeObject(forKey: "logo")
       def.removeObject(forKey: "address")
       UserDefaults.standard.synchronize()
   }
    
    func openMapOnAddress(long:String, lat:String) {
        let latitude: CLLocationDegrees = Double(lat)!
        let longitude: CLLocationDegrees = Double(long)!
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span) ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: options)
    }
    
}
