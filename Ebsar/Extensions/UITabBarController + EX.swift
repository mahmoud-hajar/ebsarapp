//
//  UITabBarController + EX.swift
//  MvvmRxSwiftProOne
//
//  Created by Ghoost on 9/19/20.
//  Copyright © 2020 Khalij. All rights reserved.
//

import UIKit

extension UITabBarController {
  
    func setGradientBackground(rightColor: UIColor, leftColor: UIColor) {
        let layerGradient = CAGradientLayer()
        layerGradient.colors = [rightColor.cgColor, leftColor.cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        self.tabBar.layer.insertSublayer(layerGradient, at:0)
    }
    
    
    
}
