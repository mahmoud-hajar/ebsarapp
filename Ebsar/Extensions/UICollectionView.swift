//
//  UICollectionView.swift
//  PizzaWorldOne
//
//  Created by Ghoost on 10/14/20.
//

import UIKit

extension UICollectionView {
    
    func registerCell<Cell: UICollectionViewCell>(cellClass: Cell.Type){
        //MARK: Generic Register cells
        self.register(UINib(nibName: String(describing: Cell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: Cell.self))
    }
    
    
    func dequeue<Cell: UICollectionViewCell>(indexPath: IndexPath) -> Cell{
        let identifier = String(describing: Cell.self)
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? Cell else {
            fatalError("Error in cell")
        }
        return cell
    }
    
    func registerHeaderFooter<Cell: UICollectionReusableView>(cellClass: Cell.Type, kind: String){
        //MARK: Generic Register Header (Header/Footer)
        self.register(UINib(nibName: String(describing: Cell.self), bundle: nil), forSupplementaryViewOfKind: kind, withReuseIdentifier: String(describing: Cell.self))
    }
    
    //    UICollectionView.elementKindSectionHeader
    
    func dequeueHeaderFooter<Cell: UICollectionReusableView>(kind: String, indexPath:IndexPath) -> Cell{
        let identifier = String(describing: Cell.self)
        guard let cell = self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: identifier, for: indexPath) as? Cell else {
            fatalError("Error in cell")
        }
        return cell
    }
////////// mahmoud
//        func autoSize(layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//            return CGSize(width: self.title?.size(withAttributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)]).width ?? 0 + 50, height: self.collectionView.frame.height - 5)
//        }
    
    
    func setCollectionLayOut(_ height:Int,_ count:Int){
            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
            layout.minimumInteritemSpacing = 2
            let num = CGFloat(count * 10)
            layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - num)/CGFloat(count), height: CGFloat(height))
             layout.scrollDirection = .vertical
            self.collectionViewLayout  = layout
        }
        
        func setCollectionWithOutHorizontalLayOut(_ height:Int,_ count:Int){
              let layout = UICollectionViewFlowLayout()
              layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            let spaces = CGFloat(count * 10)
              layout.minimumInteritemSpacing = 5
            layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - spaces)/CGFloat(count), height: CGFloat(height))
              layout.scrollDirection = .horizontal
              self.collectionViewLayout  = layout
          }
        
        func settest(_ width:CGFloat,_ height:Int,_ count:Int){
              let layout = UICollectionViewFlowLayout()
              layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            let spaces = CGFloat(count * 10)
              layout.minimumInteritemSpacing = 5
            layout.itemSize = CGSize(width: (width - spaces)/CGFloat(count), height: CGFloat(height))
             // layout.scrollDirection = .horizontal
              self.collectionViewLayout  = layout
          }
        
    

    
}
