//
//  DrawDotts.swift
//  Muetamir
//
//  Created by Ghoost on 2/24/21.
//

import UIKit

@IBDesignable class DrawDotts: UIView {

    @IBInspectable var color: UIColor = .gray

       override func layoutSubviews() {
            layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
            backgroundColor = .clear

            let shapeLayer = CAShapeLayer()
            shapeLayer.name = "DashedTopLine"
            shapeLayer.bounds = bounds
            shapeLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = color.cgColor
            shapeLayer.lineWidth = 1
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPattern = [4, 4]

            let path = CGMutablePath()
            path.move(to: CGPoint.zero)
            path.addLine(to: CGPoint(x: self.frame.width, y: 0))
            shapeLayer.path = path

            layer.addSublayer(shapeLayer)

        }
    

}
