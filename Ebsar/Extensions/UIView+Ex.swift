//
//  UIView+Ex.swift
//  Sokyakom
//
//  Created by Ghoost on 9/21/20.
//

import UIKit
import MOLH
import AVKit


extension UIView {
    
    func embedVideoPlayer(_ viewController:UIViewController,url:String) {
        let playerViewController = AVPlayerViewController()
        playerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        let videoURL = URL(string:url)
        let player = AVPlayer(url: videoURL!)
        playerViewController.player = player
        viewController.addChild(playerViewController)
        playerViewController.view.frame = self.bounds
        self.addSubview(playerViewController.view)
        playerViewController.didMove(toParent:viewController)
    }
    
    func springAnime() {
        self.transform = CGAffineTransform(scaleX: -0.1, y: 0.1)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func setRoundCorners(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func drawBorder(raduis:CGFloat, borderColor:UIColor) {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1.5
        self.setRoundCorners(raduis)
    }
    
    func shakeF() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func animateScale() {
        self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            self.transform = CGAffineTransform.identity
        }
    }
    
    
    func setGradientBackground(rightColor: UIColor,
                               leftColor: UIColor) {
        let layerGradient = CAGradientLayer()
        layerGradient.colors = [rightColor.cgColor, leftColor.cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 1.0)
        layerGradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        //layerGradient.locations = [0, 1]
        layerGradient.frame = CGRect(x: 0, y: 0, width: self.layer.bounds.width, height: self.layer.bounds.height)
        self.layer.insertSublayer(layerGradient, at:0)
    }
    
    func addTapGesture(tapNumber: Int, target: Any, action: Selector) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    
    
    func addActionn(vc:UIViewController,action: Selector){
        let tapGestureRecognizer = UITapGestureRecognizer(target:vc, action: action)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func addSwipActionn(vc:UIViewController,action: Selector){
        let swipGestureRecognizer = UISwipeGestureRecognizer(target: vc, action: action)

        if MOLHLanguage.currentAppleLanguage() == "ar" {
            swipGestureRecognizer.direction = .left
        }else {
            swipGestureRecognizer.direction = .right
        }
        
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(swipGestureRecognizer)
    }
    
    func viewOfType<T:UIView>(type:T.Type, process: (_ view:T) -> Void) {
        if let view = self as? T { process(view) } else {
            for subView in subviews {
                subView.viewOfType(type:type, process:process)
            }
        }
    }
    
    
    func roundSingleConrner(_ corners:UIRectCorner,_ cormerMask:CACornerMask, radius: CGFloat) {
        if #available(iOS 11.0, *){
            self.clipsToBounds = false
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = cormerMask
        }else{
            let rectShape = CAShapeLayer()
            rectShape.bounds = self.frame
            rectShape.position = self.center
            rectShape.path = UIBezierPath(roundedRect: self.bounds,    byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
            self.layer.mask = rectShape
        }}
    
    
    func layerGradient(startPoint:CAGradientPoint, endPoint:CAGradientPoint ,colorArray:[CGColor], type:CAGradientLayerType ) {
           let gradient = CAGradientLayer(start: startPoint, end: endPoint, colors: colorArray, type: type)
           gradient.frame.size = self.frame.size
           self.layer.insertSublayer(gradient, at: 0)
       }
    
    
    func animShow(){
           UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                          animations: {
                           self.center.y -= self.bounds.height
                           self.layoutIfNeeded()
           }, completion: nil)
           self.isHidden = false
       }
       func animHide(){
           UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                          animations: {
                           self.center.y += self.bounds.height
                           self.layoutIfNeeded()

           },  completion: {(_ completed: Bool) -> Void in
           self.isHidden = true
               })
       }
    
    
    func prepareViewConstrains(vcView: UIView, width:CGFloat, height:CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        vcView.addSubview(self)
        NSLayoutConstraint.activate([
            self.centerXAnchor.constraint(equalTo: vcView.centerXAnchor),
            self.centerYAnchor.constraint(equalTo: vcView.centerYAnchor),
            self.heightAnchor.constraint(equalToConstant: height),
            self.widthAnchor.constraint(equalToConstant: width)
        ])
    }
    
    
}

public enum CAGradientPoint {
    case topLeft
    case centerLeft
    case bottomLeft
    case topCenter
    case center
    case bottomCenter
    case topRight
    case centerRight
    case bottomRight
    var point: CGPoint {
        switch self {
        case .topLeft:
            return CGPoint(x: 0, y: 0)
        case .centerLeft:
            return CGPoint(x: 0, y: 0.5)
        case .bottomLeft:
            return CGPoint(x: 0, y: 1.0)
        case .topCenter:
            return CGPoint(x: 0.5, y: 0)
        case .center:
            return CGPoint(x: 0.5, y: 0.5)
        case .bottomCenter:
            return CGPoint(x: 0.5, y: 1.0)
        case .topRight:
            return CGPoint(x: 1.0, y: 0.0)
        case .centerRight:
            return CGPoint(x: 1.0, y: 0.5)
        case .bottomRight:
            return CGPoint(x: 1.0, y: 1.0)
        }
    }
}

extension CAGradientLayer {

    convenience init(start: CAGradientPoint, end: CAGradientPoint, colors: [CGColor], type: CAGradientLayerType) {
        self.init()
        self.frame.origin = CGPoint.zero
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors
        self.locations = (0..<colors.count).map(NSNumber.init)
        self.type = type
    }
}

class dashsView: UIView {
    
    @IBInspectable var Color: UIColor = UIColor.gray

    override func draw(_ rect: CGRect) {

//        let path = UIBezierPath()
//        // >> define the pattern & apply it
//        let dashPattern: [CGFloat] = [4.0, 4.0]
//        path.setLineDash(dashPattern, count: dashPattern.count, phase: 0)
//        // <<
//        path.lineWidth = 1
//        path.move(to: CGPoint(x: 0, y: 0))
//        path.addLine(to: CGPoint(x: 100, y: 100))
//        path.stroke()
        
        layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
        backgroundColor = .clear

        let shapeLayer = CAShapeLayer()
        shapeLayer.name = "DashedTopLine"
        shapeLayer.bounds = bounds
        shapeLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = Color.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [4, 4]

        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        shapeLayer.path = path

        layer.addSublayer(shapeLayer)

    }
}

class ViewBottomShadow: UIView {
    @IBInspectable var shadowHeight: CGFloat = 5
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
            let shadowPath = UIBezierPath()
            shadowPath.move(to: CGPoint(x: 0, y: self.bounds.height))
            shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
            shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height + shadowHeight))
            shadowPath.addLine(to: CGPoint(x: 0, y: self.bounds.height + shadowHeight))
            shadowPath.close()

            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.3
            self.layer.masksToBounds = false
            self.layer.shadowPath = shadowPath.cgPath
            self.layer.shadowRadius = 2
    }
}
