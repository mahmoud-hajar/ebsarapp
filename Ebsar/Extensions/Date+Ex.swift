//
//  Date+Ex.swift
//  Muetamir
//
//  Created by Ghoost on 3/13/21.
//

import UIKit

extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}
