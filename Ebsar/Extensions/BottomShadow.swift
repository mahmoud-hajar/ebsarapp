//
//  BottomShadow.swift
//  Muetamir
//
//  Created by Ghoost on 2/24/21.
//

import UIKit

@IBDesignable class BottomShadow: UIView {

    @IBInspectable var shadowHeight: CGFloat = 5
    
    override func layoutSubviews() {
        
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: 0, y: self.bounds.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height + shadowHeight))
        shadowPath.addLine(to: CGPoint(x: 0, y: self.bounds.height + shadowHeight))
        shadowPath.close()

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = 2
        
    }
    
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        
//            let shadowPath = UIBezierPath()
//            shadowPath.move(to: CGPoint(x: 0, y: self.bounds.height))
//            shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
//            shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height + shadowHeight))
//            shadowPath.addLine(to: CGPoint(x: 0, y: self.bounds.height + shadowHeight))
//            shadowPath.close()
//
//            self.layer.shadowColor = UIColor.black.cgColor
//            self.layer.shadowOpacity = 0.2
//            self.layer.masksToBounds = false
//            self.layer.shadowPath = shadowPath.cgPath
//            self.layer.shadowRadius = 2
//    }
    
}
