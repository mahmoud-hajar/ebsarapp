//
//  Extensions.swift
//  Sun Fun
//
//  Created by arab devolpers on 7/17/19.
//  Copyright © 2019 arab devolpers. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import FTToastIndicator
import FTNotificationIndicator
import DropDown
import ImageSlideshow
import SideMenu

extension UIViewController {
    func showAlert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "ok".localized(), style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    func is_ar()->String{
        return Bundle.main.preferredLocalizations[0]
        
    }
    func goToLoginNow(){
        let vc = UIStoryboard(name: "Main", bundle: nil)
        let rootVc = vc.instantiateViewController(withIdentifier: "loginID")
        self.present(rootVc, animated: true, completion: nil)
    }
    
    func is_login()->Bool{
        return UserDefaults.standard.bool(forKey: "is_login")
    }
    func configSliderShow(_ slideView:ImageSlideshow,_ SlideImages:[String]) {
           slideView.slideshowInterval = 3.0
           slideView.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
           slideView.contentScaleMode = UIView.ContentMode.scaleToFill
           let pageControl = UIPageControl()
           pageControl.currentPageIndicatorTintColor = UIColor(named: "MainColor")
           pageControl.pageIndicatorTintColor = UIColor.white
           slideView.pageIndicator = pageControl
           slideView.activityIndicator = DefaultActivityIndicator()
           slideView.activityIndicator = DefaultActivityIndicator(style: .gray , color: nil )
           slideView.addSubview(pageControl)
           slideShow(slideView,SlideImages)
       }
    func slideShow(_ slideView:ImageSlideshow,_ slideImages:[String]) {
        var imgSource = [InputSource]()
        for item in slideImages{

            imgSource.append(SDWebImageSource(urlString:  item)!)
        }
        
        slideView.setImageInputs(imgSource)
    }

    func drop(anchor:UIView,dataSource:[String],completion: @escaping(_ item:String,_ index:Int)->Void) {
               let dropDown = DropDown()
               dropDown.anchorView = anchor
               dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)! + 5)
               dropDown.anchorView?.plainView.semanticContentAttribute = .unspecified
               dropDown.textColor = .black
               dropDown.dataSource = dataSource
//               dropDown.RoundCorners(cornerRadius: 10.0)
//               dropDown.textFont = UIFont(name: "neoSansArabic", size: 17.0)!
               dropDown.cellHeight = 40.0
               dropDown.layer.borderWidth = 1.0
               dropDown.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               dropDown.clipsToBounds = true
        
               dropDown.selectionAction = {(index: Int, item: String) in
                   completion(item,index)
               }
               dropDown.show()
           }
     func rateApp(appId: String) {
         openUrl("itms-apps://itunes.apple.com/app/" + appId)
         }
         func openUrl(_ urlString:String) {
         let url = URL(string: urlString)!
         if #available(iOS 10.0, *) {
         UIApplication.shared.open(url, options: [:], completionHandler: nil)
         } else {
         UIApplication.shared.openURL(url)
         }
         }
    func toast(_ txt:String){
        FTToastIndicator.setToastIndicatorStyle(.dark)
        FTToastIndicator.showToastMessage(txt.localized())
    }
    func to_st(_ txt:String){
           FTToastIndicator.setToastIndicatorStyle(.dark)
           FTToastIndicator.showToastMessage(txt)
       }
    
    func openWhatsApp(_ phone_number:String){
        
                  let appURL = URL(string: "https://wa.me/\(phone_number)")!
                  if UIApplication.shared.canOpenURL(appURL) {
                      if #available(iOS 10.0, *) {
                          UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                      } else {
                          UIApplication.shared.openURL(appURL)
                      }
                  }
           
    }
    func showNoti(_ txt:String){
        FTNotificationIndicator.setNotificationIndicatorStyle(.dark)
        FTNotificationIndicator.showNotification(with: UIImage(named: "logo"), title: "Khubara".localized(), message: txt.localized())
        FTNotificationIndicator.setDefaultDismissTime(4.0)
    }
    func showNoti_without_Localization(_ txt:String){
           FTNotificationIndicator.setNotificationIndicatorStyle(.dark)
           FTNotificationIndicator.showNotification(with: UIImage(named: "logo"), title: "Khubara".localized(), message: txt)
           FTNotificationIndicator.setDefaultDismissTime(4.0)
       }
    
    func fromStampToString(time: TimeInterval , format : String) -> String {
        
        let date = Date(timeIntervalSince1970: time)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat =  format // "EEE MMM dd HH:mm:ss Z yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func ArabicNumReplacement(TF:UITextField,SS:String)->Bool {
           if TF.keyboardType == .numberPad && SS != "" {
               let numberStr: String = SS
               let formatter: NumberFormatter = NumberFormatter()
               formatter.locale = Locale(identifier: "EN")
               if let final = formatter.number(from: numberStr) {
                   TF.text =  "\(TF.text ?? "")\(final)"
               }
               return false
           }
           return true
       }
    

    func alertdone(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "ok".localized(), style: .default, handler: { action in
            _ = self.navigationController?.popToRootViewController(animated: true)
        })
       
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func loginAlert() {
        let alertController = UIAlertController(title: "Khbara".localized(), message: "loginPlease".localized(), preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "ok".localized(), style: .default, handler: { action in
             let vc = UIStoryboard(name: "Main", bundle: nil)
               let rootVc = vc.instantiateViewController(withIdentifier: "loginID")
               self.present(rootVc, animated: true, completion: nil)
        })
        alertController.addAction(OKAction)
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))

        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
    
}
class helperHelper:NSObject{
class func sideMenu(_ storyboard:String ,_ identifier:String)->SideMenuNavigationController{
        let menu = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier:identifier) as! SideMenuNavigationController
        menu.menuWidth = UIScreen.main.bounds.width/2 + UIScreen.main.bounds.width/4
        menu.presentationStyle = .menuSlideIn
        if Locale.preferredLanguages[0] == "ar" {menu.leftSide = false}else{menu.leftSide = true}
        return menu
    }
}


