//
//  UIColor+Ex.swift
//  MvvmRxSwiftProOne
//
//  Created by Ghoost on 9/19/20.
//  Copyright © 2020 Khalij. All rights reserved.
//

import UIKit

class UIColor_Ex: NSObject {
  private override init() {}
    
  static let shared = UIColor_Ex()
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
}
extension UIColor {
    
    static var appTransparent: UIColor {
        return UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.5)
    }
    
    static var AppBlue = UIColor(hex: "#217AD7", alpha: 0.5)
    static var AppTitleBlue = UIColor(hex: "#217AD7")
    static var accentColor = UIColor(hex: "#23A491", alpha: 0.5)
    static var primaryColor = UIColor(hex: "#23A491")
    
    convenience init(hex:String, alpha: CGFloat = 1)
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) { cString.remove(at: cString.startIndex) }
        
        if ((cString.count) != 6) { self.init(white: 0, alpha: alpha); return }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
}
