//
//  UILabel+Ex.swift
//  Sokyakom
//
//  Created by Ghoost on 10/4/20.
//

import UIKit
import MOLH


extension UILabel {
    
    
    
    func initLabel(title:String,titleColor :UIColor , backgroundColor:UIColor,roundCorner :CGFloat, index:ChooseFont, fontSize:CGFloat , alignment : NSTextAlignment? = nil) {
        
        if alignment != nil {
            self.textAlignment = alignment!
        }else{
            switch MOLHLanguage.currentAppleLanguage() {
            case "ar":
                self.textAlignment = .right
            default:
                self.textAlignment = .left
            }
        }
        
        self.text = title.localized()
        self.textColor = titleColor
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = roundCorner
        self.clipsToBounds = true
        self.font = UIFont(name: CustomFont(index: index), size: fontSize)
        
    }
    
    func labelLocalization() {
        switch MOLHLanguage.currentAppleLanguage() {
        case "ar":
            self.textAlignment = .right
        default:
            self.textAlignment = .left
        }
    }
    
    
    
    func estimatedHeightOfLabel() -> CGFloat {
        let size = CGSize(width: self.frame.width - 16, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)]
        let rectangleHeight = String(self.text ?? "").boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        return rectangleHeight
    }
    
    func labelHeight() -> CGFloat {
        let constraintRect = CGSize(width: self.frame.size.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.text?.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox?.height ?? 0)
      }
    
    
}
