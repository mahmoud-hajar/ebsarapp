//
//  UITextField+Ex.swift
//  Sokyakom
//
//  Created by Ghoost on 9/23/20.
//

import UIKit
import MOLH
//import iOSDropDown

extension UITextField {
    
    func placeHolderColor(color: UIColor) {
        let placeholderAttributedString = NSMutableAttributedString(attributedString: self.attributedPlaceholder!)
        placeholderAttributedString.addAttribute(.foregroundColor, value: color, range: NSRange(location: 0, length: placeholderAttributedString.length))
        self.attributedPlaceholder = placeholderAttributedString
    }
    
    
    func setImageToLeftView(img: UIImage) {
        let imageView = UIImageView(frame: CGRect(x: 8, y:8 , width: 16, height: 16))
          imageView.frame = CGRect(x: 16, y: 0, width: 24, height: 24)
        // rightView = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
           imageView.contentMode = .scaleAspectFit
           self.leftViewMode = .always
           imageView.image = img
           self.leftView = imageView
    }
    
    func setImageToRightView(img: UIImage) {
        let imageView = UIImageView(frame: CGRect(x: 8, y:8 , width: 16, height: 16))
          imageView.frame = CGRect(x: 16, y: 0, width: 24, height: 24)
        // rightView = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
           imageView.contentMode = .scaleAspectFit
           self.rightViewMode = .always
           imageView.image = img
           self.rightView = imageView
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
          let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
          self.leftView = paddingView
          self.leftViewMode = .always
      }
    
    
      func setRightPaddingPoints(_ amount:CGFloat) {
          let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
          self.rightView = paddingView
          self.rightViewMode = .always
      }
    
    
        func textLocalization() {
             switch MOLHLanguage.currentAppleLanguage() {
             case "ar":
                 self.textAlignment = .right
             default:
                 self.textAlignment = .left
             }
         }
    
    
    
    func setButtonToLeftView(img: UIImage) {
        let imageView = UIImageView(frame: CGRect(x: 8, y:8 , width: 16, height: 16))
        imageView.frame = CGRect(x: 16, y: 0, width: 24, height: 24)
        // rightView = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        imageView.contentMode = .scaleAspectFit
        self.leftViewMode = .always
        imageView.image = img
        self.leftView = imageView
    }
    
    func setButtonToRightView(img: UIImage, target: Selector) {
        let button = UIButton(type: .custom)
        button.setImage(img, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: target, for: .touchUpInside)

        self.rightView = button
        self.rightViewMode = .always
    }
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .date
        datePicker.maximumDate=Date()
        if #available(iOS 14, *) {
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "cancel".localized(), style: .plain, target: nil, action: #selector(tapCancel))
        let barButton = UIBarButtonItem(title: "done".localized(), style: .plain, target: target, action: selector)
        toolBar.setItems([cancel, flexible, barButton], animated: false)
        self.inputAccessoryView = toolBar
    }
    
    @objc func tapCancel() {
        endEditing(true)
    }
    
    
}
