//
//  SliderShowConfig.swift
//  Truck Trip
//
//  Created by motaweron on 3/4/21.
//  Copyright © 2021 motawron. All rights reserved.
//

import ImageSlideshow


extension ImageSlideshow {
    
    func setSliderConfig() {
        self.slideshowInterval = 3.0
        self.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        self.contentScaleMode = UIView.ContentMode.scaleToFill
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.blue
        self.pageIndicator = pageControl
        self.activityIndicator = DefaultActivityIndicator()
    }
    func slideShowData(_ sliderImages:[String]) {
        var imgSource = [InputSource]()
        for item in sliderImages{
            print("Darsh")
    // imgSource.append(SDWebImageSource(urlString: imageURL + item["image"].stringValue)!)
            let image = SDWebImageSource(urlString: URLs.mainImageURL + item.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            if let sdURL = image{
                imgSource.append(sdURL)
            }
        }
        self.setImageInputs(imgSource)
    }
}



