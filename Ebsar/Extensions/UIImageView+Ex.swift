//
//  UIImageView+Ex.swift
//  Sokyakom
//
//  Created by Ghoost on 10/15/20.
//

import UIKit
import MOLH
import Kingfisher

extension UIImageView {
    func LocalizedImage() {
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.image = UIImage(named: "Icon feather-arrow-right")
        }else{
            self.image = UIImage(named: "Icon feather-arrow-right2")
        }
    }
    
    func LocalizedProfileImage() {
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.image = UIImage(named: "Group1 3")
        }else{
            self.image = UIImage(named: "Group 3-1")
        }
    }
    
    
    func LocalizedHomeImage() {
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.image = UIImage(named: "home_ar")
        }else{
            self.image = UIImage(named: "home_en")
        }
    }
    
    func LocalizedLeaveOrReportRequestImage() {
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.image = UIImage(named: "GroupAR")
        }else{
            self.image = UIImage(named: "GroupEN")
        }
    }
    
    func LocalizedMoreImage() {
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.image = UIImage(named: "PathAR")
        }else{
            self.image = UIImage(named: "PathEN")
        }
    }
    
    func setImage(from url: String) {
            guard let imageURL = URL(string: url) else { return }
        self.kf.indicatorType = .activity
        self.kf.setImage(with: imageURL,placeholder: UIImage(named: "logo"),options: [.transition(ImageTransition.fade(0.5))])
      }
    
    
}
